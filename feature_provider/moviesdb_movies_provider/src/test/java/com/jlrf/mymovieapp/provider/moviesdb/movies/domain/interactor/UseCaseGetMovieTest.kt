package com.jlrf.mymovieapp.provider.moviesdb.movies.domain.interactor

import com.jlrf.base_provider.BaseModel
import com.jlrf.base_tests.TestCoroutineRule
import com.jlrf.mymovieapp.provider.moviesdb.movies.domain.MDBMoviesRepository
import com.jlrf.mymovieapp.provider.moviesdb.movies.domain.model.MDBMovieModel
import io.mockk.Runs
import io.mockk.coEvery
import io.mockk.just
import io.mockk.mockk
import io.mockk.spyk
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.flow.collect
import org.junit.Before
import org.junit.Test

internal class UseCaseGetMovieTest {

    private val repository: MDBMoviesRepository = mockk()
    private lateinit var useCase: UseCaseGetMovie
    private val coroutinesRule = TestCoroutineRule()

    @Before
    fun setUp() {
        useCase = spyk(UseCaseGetMovie(repository), recordPrivateCalls = true)
    }

    @InternalCoroutinesApi
    @Test
    fun executeUseCaseSuccess() {
        coroutinesRule.runBlockingTest {
            val modelMock = MDBMovieModel()
            val params = UseCaseGetMovie.Params(TEST_MOVIE_ID, TEST_LANGUAGE)
            coEvery { repository.getMDBMovieDetail(any(), any()) } returns modelMock
            coEvery { repository.saveMovie(any()) } just Runs
            coEvery { repository.loadMDBMovieDetail(any()) } returns modelMock
            val result = useCase.execute(params)
            result.collect {
                assert(modelMock.id == it.id)
                assert(it.errorCode == BaseModel.NO_VALUE)
            }
        }
    }

    @InternalCoroutinesApi
    @Test
    fun executeUseCaseNoSuccess() {
        coroutinesRule.runBlockingTest {
            val modelMock = MDBMovieModel()
            val params = UseCaseGetMovie.Params(TEST_MOVIE_ID, TEST_LANGUAGE)
            coEvery { repository.saveMovie(any()) } just Runs
            coEvery { repository.loadMDBMovieDetail(any()) } returns modelMock
            coEvery { repository.getMDBMovieDetail(any(), any()) } returns modelMock.apply {
                this.setError(BaseModel.COMMON_ERROR, ERROR_MESSAGE)
            }
            val result = useCase.execute(params)
            result.collect {
                assert(modelMock.id == it.id)
                if (it.status != BaseModel.LOADING) {
                    assert(it.errorCode == BaseModel.COMMON_ERROR)
                    assert(it.error)
                    assert(it.baseErrorMessage == ERROR_MESSAGE)
                }
            }
        }
    }

    companion object {
        private const val TEST_MOVIE_ID = 0
        private const val TEST_LANGUAGE = "spanish"
        private const val ERROR_MESSAGE = "The server has  a super fatal error"
    }
}
