package com.jlrf.mymovieapp.provider.moviesdb.movies.data.repository

import com.jlrf.base_tests.CoroutineTestRuleWithMockDispatchers
import com.jlrf.mymovieapp.provider.moviesdb.movies.data.repository.datasource.MDBMoviesCloudDataSource
import com.jlrf.mymovieapp.provider.moviesdb.movies.data.repository.datasource.MDBMoviesDiskDataSource
import com.jlrf.mymovieapp.provider.moviesdb.movies.domain.model.MDBMovieListModel
import com.jlrf.mymovieapp.provider.moviesdb.movies.domain.model.MDBMovieModel
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Ignore
import org.junit.Rule
import org.junit.Test

@ExperimentalCoroutinesApi
internal class MDBMoviesRepositoryTest {

    @get:Rule
    val coroutineRule = CoroutineTestRuleWithMockDispatchers()

    private val cloudDataSource: MDBMoviesCloudDataSource = mockk()
    private val diskDataSource: MDBMoviesDiskDataSource = mockk()
    private val repository = MDBMoviesRepositoryImpl(cloudDataSource, diskDataSource)
    private val moviesList: MDBMovieListModel = mockk()
    private val movieModel: MDBMovieModel = mockk()

    @Test
    fun getMDBNowPlayingMovies() {
        runBlockingTest {
            coEvery { cloudDataSource.getNowPlayingMovies(any(), any()) } returns moviesList
            val result = repository.getMDBNowPlayingMovies(TEST_PAGE, TEST_LANGUAGE)
            coVerify { cloudDataSource.getNowPlayingMovies(TEST_PAGE, TEST_LANGUAGE) }
            assert(result == moviesList)
        }
    }

    @Test
    fun getMDBMovieDetail() {
        runBlockingTest {
            coEvery { cloudDataSource.getMovieDetail(any(), any()) } returns movieModel
            val result = repository.getMDBMovieDetail(TEST_MOVIE_ID, TEST_LANGUAGE)
            coVerify { cloudDataSource.getMovieDetail(TEST_MOVIE_ID, TEST_LANGUAGE) }
            assert(result == movieModel)
        }
    }

    @Test
    fun loadMDBNowPlayingMovies() {
        runBlockingTest {
            coEvery { diskDataSource.selectNowPlayingMovies() } returns moviesList
            val result = repository.loadMDBNowPlayingMovies()
            coVerify { diskDataSource.selectNowPlayingMovies() }
            assert(result == moviesList)
        }
    }

    @Test
    fun loadMDBMovieDetail() {
        runBlockingTest {
            coEvery { diskDataSource.selectMovie(any()) } returns movieModel
            val result = repository.loadMDBMovieDetail(TEST_MOVIE_ID)
            coVerify { diskDataSource.selectMovie(TEST_MOVIE_ID) }
            assert(result == movieModel)
        }
    }

    @Test
    fun saveMovies() {
        runBlockingTest {
            coEvery { diskDataSource.insertNowPlayingMovies(any()) } returns mockk<List<Long>>()
            val mockList = mockk<List<MDBMovieModel>>()
            repository.saveNowPlayingMovies(mockList)
            coVerify { diskDataSource.insertNowPlayingMovies(mockList) }
        }
    }

    @Ignore
    fun saveMovie() {
        runBlockingTest {
            coEvery { diskDataSource.insertMovie(any()) } returns mockk<Long>()
            repository.saveMovie(movieModel)
            coVerify { diskDataSource.insertMovie(movieModel) }
        }
    }

    companion object {
        private const val TEST_PAGE = 0
        private const val TEST_MOVIE_ID = 0
        private const val TEST_LANGUAGE = "spanish"
    }
}
