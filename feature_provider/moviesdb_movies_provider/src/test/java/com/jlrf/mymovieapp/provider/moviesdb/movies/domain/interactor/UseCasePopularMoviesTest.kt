package com.jlrf.mymovieapp.provider.moviesdb.movies.domain.interactor

import com.jlrf.base_provider.BaseModel
import com.jlrf.base_tests.TestCoroutineRule
import com.jlrf.mymovieapp.provider.moviesdb.movies.domain.MDBMoviesRepository
import com.jlrf.mymovieapp.provider.moviesdb.movies.domain.model.MDBMovieListModel
import com.jlrf.network.factory.custom_callback.HttpErrorCodes
import io.mockk.Runs
import io.mockk.coEvery
import io.mockk.just
import io.mockk.mockk
import io.mockk.spyk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect
import org.junit.Before
import org.junit.Test

internal class UseCasePopularMoviesTest {

    private val coroutinesRule = TestCoroutineRule()

    private val repository: MDBMoviesRepository = mockk()

    private lateinit var useCase: UseCasePopularMovies

    @Before
    fun setUp() {
        useCase = spyk(UseCasePopularMovies(repository), recordPrivateCalls = true)
    }

    @ExperimentalCoroutinesApi
    @Test
    fun executeUseCaseSuccess() {
        coroutinesRule.runBlockingTest {
            val modelMock = MDBMovieListModel()
            val params = UseCasePopularMovies.Params(TEST_LANGUAGE, TEST_PAGE)
            coEvery { repository.savePopularMovies(any()) } just Runs
            coEvery { repository.loadPopularMovies() } returns modelMock
            coEvery { repository.getPopular(any(), any()) } returns modelMock
            val result = useCase.execute(params)
            result.collect {
                assert(modelMock.page == it.page)
                assert(it.errorCode == BaseModel.NO_VALUE)
                assert(modelMock.results.size == it.results.size)
            }
        }
    }

    @Test
    fun executeUseCaseNetworkError() {
        coroutinesRule.runBlockingTest {
            val modelMock = MDBMovieListModel()
            val params = UseCasePopularMovies.Params(TEST_LANGUAGE, TEST_PAGE)
            coEvery { repository.savePopularMovies(any()) } just Runs
            coEvery { repository.loadPopularMovies() } returns modelMock
            coEvery { repository.getPopular(any(), any()) } returns modelMock.apply {
                setError(HttpErrorCodes.NO_NETWORK_RESPONSE_CODE)
            }
            val result = useCase.execute(params)
            result.collect {
                if (it.status != BaseModel.LOADING) {
                    assert(modelMock.page == it.page)
                    assert(it.errorCode == HttpErrorCodes.NO_NETWORK_RESPONSE_CODE)
                    assert(modelMock.results.size == it.results.size)
                }
            }
        }
    }

    @ExperimentalCoroutinesApi
    @Test
    fun executeUseCaseExceptionError() {
        coroutinesRule.runBlockingTest {
            val modelMock = MDBMovieListModel()
            val params = UseCasePopularMovies.Params(TEST_LANGUAGE, TEST_PAGE)
            coEvery { repository.savePopularMovies(any()) } just Runs
            coEvery { repository.loadPopularMovies() } returns modelMock
            coEvery { repository.getPopular(any(), any()) } returns modelMock.apply {
                setError(HttpErrorCodes.EXCEPTION_RESPONSE_CODE)
            }
            val result = useCase.execute(params)
            result.collect {
                if (it.status != BaseModel.LOADING) {
                    assert(modelMock.page == it.page)
                    assert(it.errorCode == HttpErrorCodes.EXCEPTION_RESPONSE_CODE)
                    assert(modelMock.results.size == it.results.size)
                }
            }
        }
    }

    @ExperimentalCoroutinesApi
    @Test
    fun executeUseCaseServerError() {
        coroutinesRule.runBlockingTest {
            val modelMock = MDBMovieListModel()
            val params = UseCasePopularMovies.Params(TEST_LANGUAGE, TEST_PAGE)
            coEvery { repository.savePopularMovies(any()) } just Runs
            coEvery { repository.loadPopularMovies() } returns modelMock
            coEvery { repository.getPopular(any(), any()) } returns modelMock.apply {
                setError(HttpErrorCodes.INVALID_SERVER_ERROR)
            }
            val result = useCase.execute(params)
            result.collect {
                if (it.status != BaseModel.LOADING) {
                    assert(modelMock.page == it.page)
                    assert(it.errorCode == HttpErrorCodes.INVALID_SERVER_ERROR)
                    assert(modelMock.results.size == it.results.size)
                }
            }
        }
    }

    companion object {
        private const val TEST_PAGE = 0
        private const val TEST_LANGUAGE = "spanish"
    }
}
