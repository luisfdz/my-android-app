package com.jlrf.mymovieapp.provider.moviesdb.movies.data.db

import com.jlrf.base_tests.CoroutineTestRuleWithMockDispatchers
import com.jlrf.mymovieapp.provider.moviesdb.movies.data.repository.datasource.MDBMoviesDiskDataSource
import com.jlrf.mymovieapp.provider.moviesdb.movies.data.repository.mapper.MDBMoviesDataMapper
import com.jlrf.mymovieapp.provider.moviesdb.movies.domain.model.MDBMovieModel
import com.jlrf.storage.MDBDatabase
import com.jlrf.storage.entities.MovieCategoryEntity
import com.jlrf.storage.entities.MovieEntity
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import io.mockk.spyk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Ignore
import org.junit.Rule
import org.junit.Test

internal class MDBMoviesDiskDataSourceTest {

    @ExperimentalCoroutinesApi
    @get:Rule
    val coroutineRule = CoroutineTestRuleWithMockDispatchers()

    private val dataBase: MDBDatabase = mockk()
    private val mockMapper: MDBMoviesDataMapper = spyk(MDBMoviesDataMapper())
    private lateinit var diskDataSource: MDBMoviesDiskDataSource

    @Before
    fun setUp() {
        diskDataSource = MDBMoviesDiskDataSourceImpl(dataBase, mockMapper)
    }

    @ExperimentalCoroutinesApi
    @Ignore("Temporary ignored to be able to generate Gitlab Page")
    @Test
    fun selectNowPlayingMovies() {
        runBlocking {
            val movieEntity = MovieEntity()
            val moviesListEntity = listOf<MovieEntity>(movieEntity)
            val categoryEntity: MovieCategoryEntity = MovieCategoryEntity()
            val categories = listOf(categoryEntity)
            coEvery { dataBase.movieCategoryDao().loadCategoryById(any()) } returns categories
            coEvery { dataBase.movieDao().loadMovieById(any()) } returns movieEntity
            val response = diskDataSource.selectNowPlayingMovies()
            coVerify { dataBase.movieCategoryDao().loadCategoryById(MDBMoviesDataMapper.NOW_PLAYING_CATEGORY) }
            coVerify { mockMapper.convert(moviesListEntity) }
            checkNotNull(response.results)
            assert(response.results.size == moviesListEntity.size)
        }
    }

    @ExperimentalCoroutinesApi
    @Test
    fun selectMovie() {
        runBlocking {
            val movieModel = MDBMovieModel(id = TEST_MOVIE_ID)
            val movieEntity = MovieEntity(movie_id = TEST_MOVIE_ID)
            coEvery { dataBase.movieDao().loadMovieById(any()) } returns movieEntity
            coEvery { mockMapper.convert(any<MovieEntity>()) } returns movieModel
            val response = diskDataSource.selectMovie(TEST_MOVIE_ID)
            coVerify { dataBase.movieDao().loadMovieById(TEST_MOVIE_ID) }
            coVerify { mockMapper.convert(movieEntity) }
            assert(response.id == TEST_MOVIE_ID)
        }
    }

    @ExperimentalCoroutinesApi
    @Ignore("Temporary ignored to be able to generate Gitlab Page")
    @Test
    fun insertMovies() {
        runBlocking {
            val moviesLong = listOf<Long>()
            val moviesInsert = listOf<MDBMovieModel>()
            val moviesListEntity = listOf<MovieEntity>(MovieEntity())
            coEvery { dataBase.movieDao().insertAll(any()) } returns moviesLong
            coEvery { dataBase.movieCategoryDao().insertAll(any()) } returns moviesLong
            coEvery { mockMapper.convert(any<List<MDBMovieModel>>()) } returns moviesListEntity
            val response = diskDataSource.insertNowPlayingMovies(moviesInsert)
            coVerify { dataBase.movieDao().insertAll(moviesListEntity) }
            coVerify { mockMapper.convert(moviesInsert) }
            assert(response == moviesLong)
        }
    }

    @ExperimentalCoroutinesApi
    @Test
    fun insertMovie() {
        runBlocking {
            val moviesLong = 0L
            val moviesInsert = MDBMovieModel()
            val movieEntity = MovieEntity()
            coEvery { dataBase.movieDao().insert(any()) } returns moviesLong
            coEvery { mockMapper.convert(any<MDBMovieModel>()) } returns movieEntity
            val response = diskDataSource.insertMovie(moviesInsert)
            coVerify { dataBase.movieDao().insert(movieEntity) }
            coVerify { mockMapper.convert(moviesInsert) }
            assert(response == moviesLong)
        }
    }

    companion object {
        private const val TEST_MOVIE_ID = 550
    }
}
