package com.jlrf.mymovieapp.provider.moviesdb.movies.data.repository.mapper

import com.jlrf.base_provider.utils.DomainErrorConstants.PARSING_ERROR
import com.jlrf.base_tests.BaseUnitTest
import com.jlrf.mymovieapp.provider.moviesdb.movies.data.api.model.MDBMovieResponse
import com.jlrf.mymovieapp.provider.moviesdb.movies.data.api.model.MDBNowPlayingMoviesListResponse
import com.jlrf.storage.entities.MovieEntity
import io.mockk.spyk
import io.mockk.verify
import org.junit.Before
import org.junit.Test

internal class MDBMoviesDataMapperTest : BaseUnitTest() {

    private lateinit var mapper: MDBMoviesDataMapper

    @Before
    fun setUp() {
        mapper = spyk(MDBMoviesDataMapper(), recordPrivateCalls = true)
    }

    @Test
    fun convertMovieSuccess() {
        val response = readResourceFile<MDBMovieResponse>("movie_detail_success.json", MDBMovieResponse::class.java)

        val model = mapper.convert(response)
        val entity = mapper.convert(model)
        val modelData = mapper.convert(entity)

        assert(model.id == modelData.id)
        assert(model.status == modelData.status)
    }

    @Test
    fun convertMovieError() {
        val response: MDBMovieResponse? = null
        val responseEntity: MovieEntity? = null

        val model = mapper.convert(response)
        val modelData = mapper.convert(responseEntity)

        assert(model.id == modelData.id)
        assert(model.status == modelData.status)
        assert(model.errorCode == PARSING_ERROR)
        assert(model.error)
        assert(modelData.error)
    }

    @Test
    fun convertMoviesSuccess() {
        val response = readResourceFile<MDBNowPlayingMoviesListResponse>("movies_now_playing_success.json", MDBNowPlayingMoviesListResponse::class.java)

        val model = mapper.convert(response)

        verify { mapper.convert(any<MDBMovieResponse>()) }
        verify { mapper invoke "convert" withArguments listOf(response.movieDatesResponse) }
    }
}
