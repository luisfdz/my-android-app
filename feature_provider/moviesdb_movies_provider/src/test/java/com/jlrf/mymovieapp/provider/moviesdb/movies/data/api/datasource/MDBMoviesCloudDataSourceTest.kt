package com.jlrf.mymovieapp.provider.moviesdb.movies.data.api.datasource

import com.jlrf.base_tests.BaseUnitTest
import com.jlrf.base_tests.CoroutineTestRuleWithMockDispatchers
import com.jlrf.mymovieapp.provider.moviesdb.movies.data.api.MDBMoviesService
import com.jlrf.mymovieapp.provider.moviesdb.movies.data.api.model.MDBErrorResponse
import com.jlrf.mymovieapp.provider.moviesdb.movies.data.api.model.MDBMovieResponse
import com.jlrf.mymovieapp.provider.moviesdb.movies.data.api.model.MDBNowPlayingMoviesListResponse
import com.jlrf.mymovieapp.provider.moviesdb.movies.data.repository.datasource.MDBMoviesCloudDataSource
import com.jlrf.mymovieapp.provider.moviesdb.movies.data.repository.mapper.MDBMoviesDataMapper
import com.jlrf.mymovieapp.provider.moviesdb.movies.domain.model.MDBMovieListModel
import com.jlrf.mymovieapp.provider.moviesdb.movies.domain.model.MDBMovieModel
import com.jlrf.network.factory.Status
import com.jlrf.network.factory.custom_callback.HttpErrorCodes
import com.jlrf.network.factory.custom_callback_coroutines.DeferredResponse
import com.jlrf.network.factory.custom_callback_coroutines.DeferredResponse.Success
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.every
import io.mockk.mockk
import java.io.IOException
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@ExperimentalCoroutinesApi
class MDBMoviesCloudDataSourceTest : BaseUnitTest() {

    @get:Rule
    val coroutineRule = CoroutineTestRuleWithMockDispatchers()

    private val api: MDBMoviesService = mockk()
    private val mockMapper: MDBMoviesDataMapper = mockk()
    private val mapper = MDBMoviesDataMapper()

    private lateinit var cloudDataSource: MDBMoviesCloudDataSource

    @Before
    fun setUp() {
        cloudDataSource = MDBMoviesCloudDataSourceImpl(api, mockMapper)
    }

    @Test
    fun getNowPlayingMoviesSuccess() {
        val realResponse = readResourceFile<MDBNowPlayingMoviesListResponse>(
            "movies_now_playing_success.json",
            MDBNowPlayingMoviesListResponse::class.java
        )
        val moviesList = mapper.convert(realResponse)
        val deferredResponse =
            Success<MDBNowPlayingMoviesListResponse, MDBErrorResponse>(realResponse)
        runBlocking {
            coEvery { api.getNowPlaying() } returns deferredResponse
            every { mockMapper.convert(any<MDBNowPlayingMoviesListResponse>()) } returns moviesList
            val response = cloudDataSource.getNowPlayingMovies(TEST_PAGE, TEST_LANGUAGE)
            coVerify { mockMapper.convert(realResponse) }
            assert(response == moviesList)
        }
    }

    @Test
    fun getNowPlayingMoviesServerError() {
        val realResponse = readResourceFile<MDBErrorResponse>(
                "response_error.json",
                MDBErrorResponse::class.java
        )
        val moviesList = mapper.createDomainModel(
            errorCode = 404,
            clazz = MDBMovieListModel::class.java
        )
        val deferredResponse =
            DeferredResponse.ServerError<MDBNowPlayingMoviesListResponse, MDBErrorResponse>(
                errorBody = realResponse,
                code = 404
            )
        runBlocking {
            coEvery { api.getNowPlaying() } returns deferredResponse
            every {
                mockMapper.createDomainModel(
                    any(),
                    MDBMovieListModel::class.java,
                    any()
                )
            } returns moviesList
            val response = cloudDataSource.getNowPlayingMovies(TEST_PAGE, TEST_LANGUAGE)
            coVerify {
                mockMapper.createDomainModel(
                    34,
                    MDBMovieListModel::class.java,
                    Status.LOADING
                )
            }
            assert(response == moviesList)
        }
    }

    @Test
    fun getNowPlayingMoviesNetworkError() {
        val moviesList = mapper.createDomainModel(
            errorCode = 500,
            clazz = MDBMovieListModel::class.java
        )

        val deferredResponse =
            DeferredResponse.NetworkError<MDBNowPlayingMoviesListResponse, MDBErrorResponse>(
                IOException("test exception")
            )
        runBlocking {
            coEvery { api.getNowPlaying() } returns deferredResponse
            every {
                mockMapper.createDomainModel(
                    any(),
                    MDBMovieListModel::class.java,
                    any()
                )
            } returns moviesList
            val response = cloudDataSource.getNowPlayingMovies(TEST_PAGE, TEST_LANGUAGE)
            coVerify {
                mockMapper.createDomainModel(
                    HttpErrorCodes.NO_NETWORK_RESPONSE_CODE,
                    MDBMovieListModel::class.java,
                    Status.LOADING
                )
            }
            assert(response == moviesList)
        }
    }

    @Test
    fun getNowPlayingMoviesException() {
        val moviesList = mapper.createDomainModel(
            errorCode = 500,
            clazz = MDBMovieListModel::class.java
        )
        runBlocking {
            coEvery { api.getNowPlaying() } throws Exception("test exception")
            every {
                mockMapper.createDomainModel(
                    any(),
                    MDBMovieListModel::class.java,
                    any()
                )
            } returns moviesList
            val response = cloudDataSource.getNowPlayingMovies(TEST_PAGE, TEST_LANGUAGE)
            coVerify {
                mockMapper.createDomainModel(
                    HttpErrorCodes.EXCEPTION_RESPONSE_CODE,
                    MDBMovieListModel::class.java,
                    Status.LOADING
                )
            }
            assert(response == moviesList)
        }
    }

    @Test
    fun getMovieDetailSuccess() {
        val realResponse = readResourceFile<MDBMovieResponse>(
            "movie_detail_success.json",
            MDBMovieResponse::class.java
        )
        val moviesList = mapper.convert(realResponse)
        val deferredResponse =
            Success<MDBMovieResponse, MDBErrorResponse>(realResponse)
        runBlocking {
            coEvery { api.getMovie(any(), any()) } returns deferredResponse
            every { mockMapper.convert(any<MDBMovieResponse>()) } returns moviesList
            val response = cloudDataSource.getMovieDetail(TEST_MOVIE_ID, TEST_LANGUAGE)
            coVerify { api.getMovie(TEST_MOVIE_ID, TEST_LANGUAGE) }
            coVerify { mockMapper.convert(realResponse) }
            assert(response == moviesList)
        }
    }

    @Test
    fun getMovieDetailServerError() {
        val realResponse = readResourceFile<MDBErrorResponse>(
            "response_error.json",
            MDBErrorResponse::class.java
        )
        val movieDetail = mapper.createDomainModel(
            errorCode = 404,
            clazz = MDBMovieModel::class.java
        )
        val deferredResponse =
            DeferredResponse.ServerError<MDBMovieResponse, MDBErrorResponse>(
                errorBody = realResponse,
                code = 404
            )
        runBlocking {
            coEvery { api.getMovie(any(), any()) } returns deferredResponse
            every {
                mockMapper.createDomainModel(
                    any(),
                    MDBMovieModel::class.java,
                    any()
                )
            } returns movieDetail
            val response = cloudDataSource.getMovieDetail(TEST_MOVIE_ID, TEST_LANGUAGE)
            coVerify { api.getMovie(TEST_MOVIE_ID, TEST_LANGUAGE) }
            coVerify {
                mockMapper.createDomainModel(
                    34,
                    MDBMovieModel::class.java,
                    Status.LOADING
                )
            }
            assert(response == movieDetail)
        }
    }

    @Test
    fun getMovieDetailNetworkError() {
        val movieDetail = mapper.createDomainModel(
            errorCode = 500,
            clazz = MDBMovieModel::class.java
        )

        val deferredResponse =
            DeferredResponse.NetworkError<MDBMovieResponse, MDBErrorResponse>(
                IOException("test exception")
            )
        runBlocking {
            coEvery { api.getMovie(any(), any()) } returns deferredResponse
            every {
                mockMapper.createDomainModel(
                    any(),
                    MDBMovieModel::class.java,
                    any()
                )
            } returns movieDetail
            val response = cloudDataSource.getMovieDetail(TEST_MOVIE_ID, TEST_LANGUAGE)
            coVerify { api.getMovie(TEST_MOVIE_ID, TEST_LANGUAGE) }
            coVerify {
                mockMapper.createDomainModel(
                    HttpErrorCodes.NO_NETWORK_RESPONSE_CODE,
                    MDBMovieModel::class.java,
                    Status.LOADING
                )
            }
            assert(response == movieDetail)
        }
    }

    @Test
    fun getMovieDetailException() {
        val movieDetail = mapper.createDomainModel(
            errorCode = 500,
            clazz = MDBMovieModel::class.java
        )
        runBlocking {
            coEvery { api.getMovie(any(), any()) } throws Exception("test exception")
            every {
                mockMapper.createDomainModel(
                    any(),
                    MDBMovieModel::class.java,
                    any()
                )
            } returns movieDetail
            val response = cloudDataSource.getMovieDetail(TEST_MOVIE_ID, TEST_LANGUAGE)
            coVerify { api.getMovie(TEST_MOVIE_ID, TEST_LANGUAGE) }
            coVerify {
                mockMapper.createDomainModel(
                    HttpErrorCodes.EXCEPTION_RESPONSE_CODE,
                    MDBMovieModel::class.java,
                    Status.LOADING
                )
            }
            assert(response == movieDetail)
        }
    }

    companion object {
        private const val TEST_PAGE = 0
        private const val TEST_MOVIE_ID = 550
        private const val TEST_LANGUAGE = "spanish"
    }
}
