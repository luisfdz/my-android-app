package com.jlrf.mymovieapp.provider.moviesdb.movies.data.api.datasource

import com.jlrf.mymovieapp.provider.moviesdb.movies.data.api.MDBMoviesService
import com.jlrf.mymovieapp.provider.moviesdb.movies.data.repository.datasource.MDBMoviesCloudDataSource
import com.jlrf.mymovieapp.provider.moviesdb.movies.data.repository.mapper.MDBMoviesDataMapper
import com.jlrf.mymovieapp.provider.moviesdb.movies.domain.model.MDBMovieListModel
import com.jlrf.mymovieapp.provider.moviesdb.movies.domain.model.MDBMovieModel
import com.jlrf.network.factory.BaseCloudDataSource
import com.jlrf.network.factory.Status
import javax.inject.Inject

class MDBMoviesCloudDataSourceImpl @Inject constructor(
    private val api: MDBMoviesService,
    private val mapperMDB: MDBMoviesDataMapper
) : MDBMoviesCloudDataSource, BaseCloudDataSource() {

    override suspend fun getNowPlayingMovies(
        page: Int,
        language: String
    ): MDBMovieListModel {
        val result = getResultCoroutines { api.getNowPlaying() }
        return when (result.status) {
            Status.SUCCESS -> mapperMDB.convert(result.body)
            else -> mapperMDB.createDomainModel(
                errorCode = result.errorBody?.statusCode ?: result.errorCode,
                clazz = MDBMovieListModel::class.java
            )
        }
    }

    override suspend fun getPopular(page: Int, language: String): MDBMovieListModel {
        val result = getResultCoroutines { api.getPopular() }
        return when (result.status) {
            Status.SUCCESS -> mapperMDB.convert(result.body)
            else -> mapperMDB.createDomainModel(
                errorCode = result.errorBody?.statusCode ?: result.errorCode,
                clazz = MDBMovieListModel::class.java
            )
        }
    }

    override suspend fun getTopRated(page: Int, language: String): MDBMovieListModel {
        val result = getResultCoroutines { api.getTopRated() }
        return when (result.status) {
            Status.SUCCESS -> mapperMDB.convert(result.body)
            else -> mapperMDB.createDomainModel(
                errorCode = result.errorBody?.statusCode ?: result.errorCode,
                clazz = MDBMovieListModel::class.java
            )
        }
    }

    override suspend fun getUpcoming(page: Int, language: String): MDBMovieListModel {
        val result = getResultCoroutines { api.getUpcoming() }
        return when (result.status) {
            Status.SUCCESS -> mapperMDB.convert(result.body)
            else -> mapperMDB.createDomainModel(
                errorCode = result.errorBody?.statusCode ?: result.errorCode,
                clazz = MDBMovieListModel::class.java
            )
        } }

    override suspend fun getMovieDetail(movieId: Int, language: String): MDBMovieModel {
        val result = getResultCoroutines { api.getMovie(movieId, language) }
        return when (result.status) {
            Status.SUCCESS -> mapperMDB.convert(result.body)
            else -> mapperMDB.createDomainModel(
                errorCode = result.errorBody?.statusCode ?: result.errorCode,
                clazz = MDBMovieModel::class.java
            )
        }
    }
}
