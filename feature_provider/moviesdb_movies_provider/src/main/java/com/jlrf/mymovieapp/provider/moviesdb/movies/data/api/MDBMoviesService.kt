package com.jlrf.mymovieapp.provider.moviesdb.movies.data.api

import com.jlrf.mymovieapp.provider.moviesdb.movies.data.api.model.MDBErrorResponse
import com.jlrf.mymovieapp.provider.moviesdb.movies.data.api.model.MDBMovieResponse
import com.jlrf.mymovieapp.provider.moviesdb.movies.data.api.model.MDBNowPlayingMoviesListResponse
import com.jlrf.network.factory.custom_callback_coroutines.DeferredResponse
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface MDBMoviesService {

    @GET("/3/movie/now_playing")
    suspend fun getNowPlaying(
        @Query("language") language: String = "en-US",
        @Query("page") page: Int = 1
    ): DeferredResponse<MDBNowPlayingMoviesListResponse, MDBErrorResponse>

    @GET("/3/movie/popular")
    suspend fun getPopular(
        @Query("language") language: String = "en-US",
        @Query("page") page: Int = 1
    ): DeferredResponse<MDBNowPlayingMoviesListResponse, MDBErrorResponse>

    @GET("/3/movie/top_rated")
    suspend fun getTopRated(
        @Query("language") language: String = "en-US",
        @Query("page") page: Int = 1
    ): DeferredResponse<MDBNowPlayingMoviesListResponse, MDBErrorResponse>

    @GET("/3/movie/upcoming")
    suspend fun getUpcoming(
        @Query("language") language: String = "en-US",
        @Query("page") page: Int = 1
    ): DeferredResponse<MDBNowPlayingMoviesListResponse, MDBErrorResponse>

    @GET("/3/movie/{movie_id}")
    suspend fun getMovie(
        @Path("movie_id") movieId: Int,
        @Query("language") language: String = "en-US"
    ): DeferredResponse<MDBMovieResponse, MDBErrorResponse>
}
