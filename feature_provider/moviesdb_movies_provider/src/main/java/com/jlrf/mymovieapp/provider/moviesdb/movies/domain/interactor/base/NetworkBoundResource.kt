package com.jlrf.mymovieapp.provider.moviesdb.movies.domain.interactor.base

import com.jlrf.base_provider.BaseModel
import com.jlrf.network.factory.custom_callback.HttpErrorCodes
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

abstract class NetworkBoundResource<ModelType : BaseModel, R> {

    fun execute(params: R): Flow<ModelType> = flow {
        emit(getLoadingObject())
        val cloudSource = createCall(params)
        if (shouldLoad(params, cloudSource) && (cloudSource.errorCode == HttpErrorCodes.NO_NETWORK_RESPONSE_CODE || cloudSource.errorCode == HttpErrorCodes.EXCEPTION_RESPONSE_CODE) && cloudSource.error) {
            emit(loadFromDb(params))
        } else {
            saveCallResult(cloudSource)
            emit(cloudSource)
        }
    }

    protected abstract suspend fun saveCallResult(item: ModelType)

    protected fun shouldFetch(
        data: ModelType?,
        time: Long = cacheThresholdMillis
    ): Boolean = true

    protected abstract fun shouldLoad(params: R, data: ModelType): Boolean

    protected abstract suspend fun loadFromDb(params: R): ModelType

    protected abstract suspend fun createCall(params: R): ModelType

    protected abstract fun getLoadingObject(): ModelType

    companion object {
        private const val cacheThresholdMillis = 3 * 3600000L // 3 hours//
    }
}
