package com.jlrf.mymovieapp.provider.moviesdb.movies.di

import com.jlrf.mymovieapp.provider.moviesdb.movies.data.db.MDBMoviesDiskDataSourceImpl
import com.jlrf.mymovieapp.provider.moviesdb.movies.data.repository.datasource.MDBMoviesDiskDataSource
import com.jlrf.mymovieapp.provider.moviesdb.movies.data.repository.mapper.MDBMoviesDataMapper
import com.jlrf.storage.MDBDatabase
import com.jlrf.storage.di.StorageModule
import dagger.Module
import dagger.Provides

@Module(includes = [StorageModule::class])
class MDBMoviesDiskDataSourceModule {

    @Provides
    fun provideMoviesDbDiskDataSource(
        database: MDBDatabase,
        mapper: MDBMoviesDataMapper
    ): MDBMoviesDiskDataSource =
        MDBMoviesDiskDataSourceImpl(database, mapper)
}
