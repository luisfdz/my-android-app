package com.jlrf.mymovieapp.provider.moviesdb.movies.data.repository.mapper

import com.jlrf.base_provider.BaseDataMapper
import com.jlrf.base_provider.BaseModel.Companion.EMPTY_BOOLEAN
import com.jlrf.base_provider.BaseModel.Companion.EMPTY_DOUBLE
import com.jlrf.base_provider.BaseModel.Companion.EMPTY_INT
import com.jlrf.base_provider.BaseModel.Companion.EMPTY_STRING
import com.jlrf.base_provider.utils.DomainErrorConstants.PARSING_ERROR
import com.jlrf.mymovieapp.provider.moviesdb.movies.data.api.model.MDBDatesResponse
import com.jlrf.mymovieapp.provider.moviesdb.movies.data.api.model.MDBMovieResponse
import com.jlrf.mymovieapp.provider.moviesdb.movies.data.api.model.MDBNowPlayingMoviesListResponse
import com.jlrf.mymovieapp.provider.moviesdb.movies.domain.model.MDBDateModel
import com.jlrf.mymovieapp.provider.moviesdb.movies.domain.model.MDBMovieListModel
import com.jlrf.mymovieapp.provider.moviesdb.movies.domain.model.MDBMovieModel
import com.jlrf.storage.entities.MovieCategoryEntity
import com.jlrf.storage.entities.MovieEntity
import javax.inject.Inject

class MDBMoviesDataMapper @Inject constructor() :
    BaseDataMapper() {

    fun convert(response: MDBMovieResponse?): MDBMovieModel = MDBMovieModel(
        id = response?.id ?: EMPTY_INT,
        title = response?.title ?: EMPTY_STRING,
        movieStatus = response?.status ?: EMPTY_STRING,
        posterPath = response?.posterPath?.let { BASE_URL.plus(it) } ?: EMPTY_STRING,
        backdropPath = response?.backdropPath?.let { BASE_URL.plus(it) } ?: EMPTY_STRING,
        video = response?.video ?: EMPTY_BOOLEAN,
        imdbId = response?.imdbId ?: EMPTY_STRING,
        homePage = response?.homepage ?: EMPTY_STRING,
        isAdult = response?.adult ?: EMPTY_BOOLEAN,
        tagline = response?.tagline ?: EMPTY_STRING,
        voteAverage = response?.voteAverage ?: EMPTY_DOUBLE,
        releaseDate = response?.releaseDate,
        runTime = response?.runtime ?: EMPTY_INT,
        originalTitle = response?.originalTitle ?: EMPTY_STRING,
        overview = response?.overview ?: EMPTY_STRING,
        budget = response?.budget ?: EMPTY_INT,
        voteCount = response?.voteCount ?: EMPTY_INT,
        popularity = response?.popularity ?: EMPTY_DOUBLE,
        genreIds = response?.genreIds ?: listOf(),
        revenue = response?.revenue ?: EMPTY_INT,
        originalLanguage = response?.originalLanguage ?: EMPTY_STRING
    ).apply {
        if (response == null) {
            setError(PARSING_ERROR)
        } else {
            setSuccess()
        }
    }

    // TODO: Will be removed one the real implementation has been done 👍
//    fun convert(response: MDBProductionCompaniesItemResponse): MDBProductionCompanyModel =
//        MDBProductionCompanyModel()
//
//    fun convert(response: MDBSpokenLanguagesItemResponse): MDBSpokenLanguageModel =
//        MDBSpokenLanguageModel()
//
//    fun convert(response: MDBProductionCountriesItemResponse): MDBProductionCountryModel =
//        MDBProductionCountryModel()
//
//    fun convert(response: MDBGenresItemResponse): MDBGenreModel = MDBGenreModel()

    private fun convert(response: MDBDatesResponse?): MDBDateModel = MDBDateModel()

    fun convert(response: MDBNowPlayingMoviesListResponse?): MDBMovieListModel =
        MDBMovieListModel(
            results = response?.results?.map { convert(it) } ?: listOf(),
            totalResults = response?.totalResults ?: EMPTY_INT,
            totalPages = response?.totalPages ?: EMPTY_INT,
            movieDates = convert(response?.movieDatesResponse),
            page = response?.page ?: EMPTY_INT
        ).apply {
            if (response != null) {
                setSuccess()
            } else {
                setError(PARSING_ERROR)
            }
        }

    fun convert(entity: List<MovieEntity>?): MDBMovieListModel =
        MDBMovieListModel(
            results = entity?.map { convert(it) } ?: listOf()
        ).apply {
            if (entity != null) {
                setSuccess()
            } else {
                setError(PARSING_ERROR)
            }
        }

    fun convert(model: List<MDBMovieModel>): List<MovieEntity> =
        model.map { convert(it) }

    fun convert(model: List<MDBMovieModel>, category: Int): List<MovieCategoryEntity> =
        model.map { convert(it, category) }

    private fun convert(model: MDBMovieModel, category: Int): MovieCategoryEntity =
        MovieCategoryEntity(movie_id = model.id, category_id = category)

    fun convert(entity: MovieEntity?): MDBMovieModel =
        MDBMovieModel(
            id = entity?.movie_id ?: EMPTY_INT,
            title = entity?.title ?: EMPTY_STRING,
            movieStatus = entity?.status ?: EMPTY_STRING,
            posterPath = entity?.posterPath ?: EMPTY_STRING,
            backdropPath = entity?.backdropPath ?: EMPTY_STRING,
            video = entity?.video ?: EMPTY_BOOLEAN,
            imdbId = entity?.imdbId ?: EMPTY_STRING,
            homePage = entity?.homePage ?: EMPTY_STRING,
            isAdult = entity?.adult ?: EMPTY_BOOLEAN,
            tagline = entity?.tagline ?: EMPTY_STRING,
            voteAverage = entity?.voteAverage ?: EMPTY_DOUBLE,
            releaseDate = entity?.releaseDate,
            runTime = entity?.runTime ?: EMPTY_INT,
            originalTitle = entity?.originalTitle ?: EMPTY_STRING,
            overview = entity?.overview ?: EMPTY_STRING,
            budget = entity?.budget ?: EMPTY_INT,
            voteCount = entity?.voteCount ?: EMPTY_INT,
            popularity = entity?.popularity ?: EMPTY_DOUBLE,
            genreIds = entity?.genreIds ?: listOf(),
            originalLanguage = entity?.originalLanguage ?: EMPTY_STRING
        ).apply {
            if (entity == null) {
                setError(PARSING_ERROR)
            } else {
                setSuccess()
            }
        }

    fun convert(model: MDBMovieModel): MovieEntity =
        MovieEntity(
            movie_id = model.id,
            title = model.title,
            status = model.movieStatus,
            posterPath = model.posterPath,
            backdropPath = model.backdropPath,
            video = model.video,
            imdbId = model.imdbId,
            homePage = model.homePage,
            adult = model.isAdult,
            tagline = model.tagline,
            voteAverage = model.voteAverage,
            releaseDate = model.releaseDate,
            runTime = model.runTime,
            originalTitle = model.originalTitle,
            overview = model.overview,
            budget = model.budget,
            voteCount = model.voteCount,
            popularity = model.popularity,
            genreIds = model.genreIds,
            originalLanguage = model.originalLanguage
        )

    companion object {
        const val BASE_URL = "https://image.tmdb.org/t/p/original"
        const val NOW_PLAYING_CATEGORY = 0
        const val POPULAR_CATEGORRY = 1
        const val TOP_RATED_CATEGORY = 2
        const val UPCOMING_CATEGORY = 3
    }
}
