package com.jlrf.mymovieapp.provider.moviesdb.movies.di

import com.jlrf.base_provider.di.BaseProviderModule
import com.jlrf.mymovieapp.provider.moviesdb.movies.data.api.MDBMoviesService
import com.jlrf.mymovieapp.provider.moviesdb.movies.data.repository.MDBMoviesRepositoryImpl
import com.jlrf.mymovieapp.provider.moviesdb.movies.domain.MDBMoviesRepository
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

@Module(includes = [MDBMoviesCloudDataSourceModule::class, MDBMoviesDiskDataSourceModule::class, BaseProviderModule::class])
class MDBMoviesProviderModule {

    @Provides
    fun provideMovieDbService(retrofit: Retrofit): MDBMoviesService =
        retrofit.create(MDBMoviesService::class.java)

    @Provides
    fun provideMoviesDbMoviesRepository(repository: MDBMoviesRepositoryImpl): MDBMoviesRepository =
        repository
}
