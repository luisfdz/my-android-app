package com.jlrf.mymovieapp.provider.moviesdb.movies.domain.model

import com.jlrf.base_provider.BaseModel
import java.util.Date

data class MDBMovieListModel(
    val results: List<MDBMovieModel> = listOf(),
    val totalResults: Int = EMPTY_INT,
    val page: Int = EMPTY_INT,
    val totalPages: Int = EMPTY_INT,
    val movieDates: MDBDateModel = MDBDateModel()
) : BaseModel()

data class MDBMovieModel(
    val id: Int = EMPTY_INT,
    val title: String = EMPTY_STRING,
    val movieStatus: String = EMPTY_STRING,
    val posterPath: String = EMPTY_STRING,
    val backdropPath: String = EMPTY_STRING,
    val video: Boolean = EMPTY_BOOLEAN,
    val imdbId: String = EMPTY_STRING,
    val homePage: String = EMPTY_STRING,
    val isAdult: Boolean = EMPTY_BOOLEAN,
    val tagline: String = EMPTY_STRING,
    val voteAverage: Double = EMPTY_DOUBLE,
    val releaseDate: Date? = null,
    val productionCompanies: List<MDBProductionCompanyModel> = listOf(),
    val spokenLanguages: List<MDBSpokenLanguageModel> = listOf(),
    val runTime: Int = EMPTY_INT,
    val originalTitle: String = EMPTY_STRING,
    val overview: String = EMPTY_STRING,
    val budget: Int = EMPTY_INT,
    val voteCount: Int = EMPTY_INT,
    val productionCompaniesCountries: List<MDBProductionCountryModel> = listOf(),
    val popularity: Double = EMPTY_DOUBLE,
    val genreIds: List<Int> = listOf(),
    val genre: List<MDBGenreModel> = listOf(),
    val revenue: Int = EMPTY_INT,
    val originalLanguage: String = EMPTY_STRING
) : BaseModel()

data class MDBProductionCompanyModel(
    val id: Int = EMPTY_INT,
    val name: String = EMPTY_STRING,
    val logoPath: String = EMPTY_STRING,
    val originCountry: String = EMPTY_STRING
) : BaseModel()

data class MDBSpokenLanguageModel(
    val iso6391: String = EMPTY_STRING,
    val name: String = EMPTY_STRING
) : BaseModel()

data class MDBProductionCountryModel(
    val iso31661: String = EMPTY_STRING,
    val name: String = EMPTY_STRING
) : BaseModel()

data class MDBGenreModel(
    val id: Int = EMPTY_INT,
    val name: String = EMPTY_STRING
) : BaseModel()

data class MDBDateModel(
    val maximum: String = EMPTY_STRING,
    val minimum: String = EMPTY_STRING
) : BaseModel()
