package com.jlrf.mymovieapp.provider.moviesdb.movies.data.repository

import com.jlrf.mymovieapp.provider.moviesdb.movies.data.repository.datasource.MDBMoviesCloudDataSource
import com.jlrf.mymovieapp.provider.moviesdb.movies.data.repository.datasource.MDBMoviesDiskDataSource
import com.jlrf.mymovieapp.provider.moviesdb.movies.domain.MDBMoviesRepository
import com.jlrf.mymovieapp.provider.moviesdb.movies.domain.model.MDBMovieListModel
import com.jlrf.mymovieapp.provider.moviesdb.movies.domain.model.MDBMovieModel
import javax.inject.Inject

class MDBMoviesRepositoryImpl @Inject constructor(
    private val cloudDataSourceMDB: MDBMoviesCloudDataSource,
    private val diskDataSourceMDB: MDBMoviesDiskDataSource
) : MDBMoviesRepository {

    override suspend fun getMDBNowPlayingMovies(
        page: Int,
        language: String
    ): MDBMovieListModel = cloudDataSourceMDB.getNowPlayingMovies(page, language)

    override suspend fun getPopular(page: Int, language: String): MDBMovieListModel = cloudDataSourceMDB.getPopular(page, language)

    override suspend fun getTopRated(page: Int, language: String): MDBMovieListModel = cloudDataSourceMDB.getTopRated(page, language)

    override suspend fun getUpcoming(page: Int, language: String): MDBMovieListModel = cloudDataSourceMDB.getUpcoming(page, language)

    override suspend fun getMDBMovieDetail(movieId: Int, language: String): MDBMovieModel =
        cloudDataSourceMDB.getMovieDetail(movieId, language)

    override suspend fun loadMDBNowPlayingMovies(): MDBMovieListModel =
        diskDataSourceMDB.selectNowPlayingMovies()

    override suspend fun loadMDBMovieDetail(movieId: Int): MDBMovieModel =
        diskDataSourceMDB.selectMovie(movieId)

    override suspend fun saveNowPlayingMovies(movies: List<MDBMovieModel>) {
        diskDataSourceMDB.insertNowPlayingMovies(movies)
    }

    override suspend fun saveMovie(movie: MDBMovieModel) {
        diskDataSourceMDB.insertMovie(movie)
    }

    override suspend fun loadPopularMovies(): MDBMovieListModel = diskDataSourceMDB.selectPopularMovies()

    override suspend fun loadTopRatedMovies(): MDBMovieListModel = diskDataSourceMDB.selectTopRatedMovies()

    override suspend fun loadUpcomingMovies(): MDBMovieListModel = diskDataSourceMDB.selectUpcomingMovies()

    override suspend fun savePopularMovies(movies: List<MDBMovieModel>) {
        diskDataSourceMDB.insertPopularMovies(movies)
    }

    override suspend fun saveTopRatedMovies(movies: List<MDBMovieModel>) {
        diskDataSourceMDB.insertTopRatedMovies(movies)
    }

    override suspend fun saveUpconmingMovies(movies: List<MDBMovieModel>) {
        diskDataSourceMDB.insertUpconmingMovies(movies)
    }
}
