package com.jlrf.mymovieapp.provider.moviesdb.movies.domain

import com.jlrf.mymovieapp.provider.moviesdb.movies.domain.model.MDBMovieListModel
import com.jlrf.mymovieapp.provider.moviesdb.movies.domain.model.MDBMovieModel

interface MDBMoviesRepository {

    suspend fun getMDBNowPlayingMovies(
        page: Int = 1,
        language: String = "en-US"
    ): MDBMovieListModel

    suspend fun getPopular(
        page: Int = 1,
        language: String = "en-US"
    ): MDBMovieListModel

    suspend fun getTopRated(
        page: Int = 1,
        language: String = "en-US"
    ): MDBMovieListModel

    suspend fun getUpcoming(
        page: Int = 1,
        language: String = "en-US"
    ): MDBMovieListModel

    suspend fun getMDBMovieDetail(
        movieId: Int,
        language: String = "en-US"
    ): MDBMovieModel

    suspend fun loadMDBNowPlayingMovies(): MDBMovieListModel

    suspend fun loadMDBMovieDetail(movieId: Int): MDBMovieModel

    suspend fun loadPopularMovies(): MDBMovieListModel

    suspend fun loadTopRatedMovies(): MDBMovieListModel

    suspend fun loadUpcomingMovies(): MDBMovieListModel

    suspend fun saveNowPlayingMovies(movies: List<MDBMovieModel>)

    suspend fun savePopularMovies(movies: List<MDBMovieModel>)

    suspend fun saveTopRatedMovies(movies: List<MDBMovieModel>)

    suspend fun saveUpconmingMovies(movies: List<MDBMovieModel>)

    suspend fun saveMovie(movie: MDBMovieModel)
}
