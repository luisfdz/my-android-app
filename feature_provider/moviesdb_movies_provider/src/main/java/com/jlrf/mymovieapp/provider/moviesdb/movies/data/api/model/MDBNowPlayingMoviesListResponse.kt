package com.jlrf.mymovieapp.provider.moviesdb.movies.data.api.model

import com.google.gson.annotations.SerializedName

data class MDBNowPlayingMoviesListResponse(
    @field:SerializedName("dates")
    val movieDatesResponse: MDBDatesResponse? = null,

    @field:SerializedName("page")
    val page: Int? = null,

    @field:SerializedName("total_pages")
    val totalPages: Int? = null,

    @field:SerializedName("results")
    val results: List<MDBMovieResponse>? = null,

    @field:SerializedName("total_results")
    val totalResults: Int? = null
)
