package com.jlrf.mymovieapp.provider.moviesdb.movies.data.repository.datasource

import com.jlrf.mymovieapp.provider.moviesdb.movies.domain.model.MDBMovieListModel
import com.jlrf.mymovieapp.provider.moviesdb.movies.domain.model.MDBMovieModel

interface MDBMoviesCloudDataSource {

    suspend fun getNowPlayingMovies(
        page: Int,
        language: String = "en-US"
    ): MDBMovieListModel

    suspend fun getPopular(
        page: Int,
        language: String = "en-US"
    ): MDBMovieListModel

    suspend fun getTopRated(
        page: Int,
        language: String = "en-US"
    ): MDBMovieListModel

    suspend fun getUpcoming(
        page: Int,
        language: String = "en-US"
    ): MDBMovieListModel

    suspend fun getMovieDetail(
        movieId: Int,
        language: String = "en-US"
    ): MDBMovieModel
}
