package com.jlrf.mymovieapp.provider.moviesdb.movies.data.api.model

import com.google.gson.annotations.SerializedName

data class MDBDatesResponse(

    @field:SerializedName("maximum")
    val maximum: String? = null,

    @field:SerializedName("minimum")
    val minimum: String? = null
)
