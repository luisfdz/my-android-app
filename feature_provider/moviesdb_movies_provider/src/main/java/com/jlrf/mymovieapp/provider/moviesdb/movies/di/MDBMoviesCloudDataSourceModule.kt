package com.jlrf.mymovieapp.provider.moviesdb.movies.di

import com.jlrf.mymovieapp.provider.moviesdb.movies.data.api.MDBMoviesService
import com.jlrf.mymovieapp.provider.moviesdb.movies.data.api.datasource.MDBMoviesCloudDataSourceImpl
import com.jlrf.mymovieapp.provider.moviesdb.movies.data.repository.datasource.MDBMoviesCloudDataSource
import com.jlrf.mymovieapp.provider.moviesdb.movies.data.repository.mapper.MDBMoviesDataMapper
import com.jlrf.network.di.NetworkModule
import dagger.Module
import dagger.Provides

@Module(includes = [NetworkModule::class])
class MDBMoviesCloudDataSourceModule {

    @Provides
    fun provideMoviesDbMoviesCloudDataSource(
        api: MDBMoviesService,
        mapper: MDBMoviesDataMapper
    ): MDBMoviesCloudDataSource =
        MDBMoviesCloudDataSourceImpl(
            api,
            mapper
        )
}
