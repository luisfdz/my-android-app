package com.jlrf.mymovieapp.provider.moviesdb.movies.data.db

import com.jlrf.mymovieapp.provider.moviesdb.movies.data.repository.datasource.MDBMoviesDiskDataSource
import com.jlrf.mymovieapp.provider.moviesdb.movies.data.repository.mapper.MDBMoviesDataMapper
import com.jlrf.mymovieapp.provider.moviesdb.movies.data.repository.mapper.MDBMoviesDataMapper.Companion.NOW_PLAYING_CATEGORY
import com.jlrf.mymovieapp.provider.moviesdb.movies.data.repository.mapper.MDBMoviesDataMapper.Companion.POPULAR_CATEGORRY
import com.jlrf.mymovieapp.provider.moviesdb.movies.data.repository.mapper.MDBMoviesDataMapper.Companion.TOP_RATED_CATEGORY
import com.jlrf.mymovieapp.provider.moviesdb.movies.data.repository.mapper.MDBMoviesDataMapper.Companion.UPCOMING_CATEGORY
import com.jlrf.mymovieapp.provider.moviesdb.movies.domain.model.MDBMovieListModel
import com.jlrf.mymovieapp.provider.moviesdb.movies.domain.model.MDBMovieModel
import com.jlrf.storage.MDBDatabase
import com.jlrf.storage.entities.MovieEntity
import javax.inject.Inject

class MDBMoviesDiskDataSourceImpl @Inject constructor(
    private val database: MDBDatabase,
    private val mapperMDB: MDBMoviesDataMapper
) : MDBMoviesDiskDataSource {

    override suspend fun selectNowPlayingMovies(): MDBMovieListModel =
        selectMoviesByCategory(NOW_PLAYING_CATEGORY)

    override suspend fun selectPopularMovies(): MDBMovieListModel =
        selectMoviesByCategory(POPULAR_CATEGORRY)

    override suspend fun selectTopRatedMovies(): MDBMovieListModel =
        selectMoviesByCategory(TOP_RATED_CATEGORY)

    override suspend fun selectUpcomingMovies(): MDBMovieListModel =
        selectMoviesByCategory(UPCOMING_CATEGORY)

    private suspend fun selectMoviesByCategory(category: Int): MDBMovieListModel {
        val categories = database.movieCategoryDao().loadCategoryById(category)
        val movies =
            categories?.map { database.movieDao().loadMovieById(it.movie_id) ?: MovieEntity() }
                ?.toList() ?: arrayListOf()
        return mapperMDB.convert(movies)
    }

    override suspend fun selectMovie(movieId: Int): MDBMovieModel =
        mapperMDB.convert(database.movieDao().loadMovieById(movieId))

    override suspend fun insertNowPlayingMovies(entities: List<MDBMovieModel>): List<Long> {
        database.movieCategoryDao().insertAll(mapperMDB.convert(entities, NOW_PLAYING_CATEGORY))
        return database.movieDao().insertAll(mapperMDB.convert(entities))
    }

    override suspend fun insertMovie(entity: MDBMovieModel): Long =
        database.movieDao().insert(mapperMDB.convert(entity))

    override suspend fun insertPopularMovies(entities: List<MDBMovieModel>): List<Long> {
        database.movieCategoryDao().insertAll(mapperMDB.convert(entities, POPULAR_CATEGORRY))
        return database.movieDao().insertAll(mapperMDB.convert(entities))
    }

    override suspend fun insertTopRatedMovies(entities: List<MDBMovieModel>): List<Long> {
        database.movieCategoryDao().insertAll(mapperMDB.convert(entities, TOP_RATED_CATEGORY))
        return database.movieDao().insertAll(mapperMDB.convert(entities))
    }

    override suspend fun insertUpconmingMovies(entities: List<MDBMovieModel>): List<Long> {
        database.movieCategoryDao().insertAll(mapperMDB.convert(entities, UPCOMING_CATEGORY))
        return database.movieDao().insertAll(mapperMDB.convert(entities))
    }
}
