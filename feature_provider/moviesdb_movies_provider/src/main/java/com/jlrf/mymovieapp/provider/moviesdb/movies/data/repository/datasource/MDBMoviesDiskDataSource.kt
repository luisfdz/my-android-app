package com.jlrf.mymovieapp.provider.moviesdb.movies.data.repository.datasource

import com.jlrf.mymovieapp.provider.moviesdb.movies.domain.model.MDBMovieListModel
import com.jlrf.mymovieapp.provider.moviesdb.movies.domain.model.MDBMovieModel

interface MDBMoviesDiskDataSource {

    suspend fun selectNowPlayingMovies(): MDBMovieListModel

    suspend fun selectPopularMovies(): MDBMovieListModel

    suspend fun selectTopRatedMovies(): MDBMovieListModel

    suspend fun selectUpcomingMovies(): MDBMovieListModel

    suspend fun selectMovie(movieId: Int): MDBMovieModel

    suspend fun insertNowPlayingMovies(entities: List<MDBMovieModel>): List<Long>

    suspend fun insertMovie(entity: MDBMovieModel): Long

    suspend fun insertPopularMovies(entities: List<MDBMovieModel>): List<Long>

    suspend fun insertTopRatedMovies(entities: List<MDBMovieModel>): List<Long>

    suspend fun insertUpconmingMovies(entities: List<MDBMovieModel>): List<Long>
}
