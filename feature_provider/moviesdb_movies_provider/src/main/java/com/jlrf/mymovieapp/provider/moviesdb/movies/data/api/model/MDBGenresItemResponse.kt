package com.jlrf.mymovieapp.provider.moviesdb.movies.data.api.model

import com.google.gson.annotations.SerializedName

data class MDBGenresItemResponse(

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("id")
    val id: Int? = null
)
