package com.jlrf.mymovieapp.provider.moviesdb.movies.domain.interactor

import com.jlrf.mymovieapp.provider.moviesdb.movies.domain.MDBMoviesRepository
import com.jlrf.mymovieapp.provider.moviesdb.movies.domain.interactor.base.NetworkBoundResource
import com.jlrf.mymovieapp.provider.moviesdb.movies.domain.model.MDBMovieModel
import com.jlrf.network.factory.custom_callback.HttpBaseValues
import javax.inject.Inject

class UseCaseGetMovie @Inject constructor(
    private val repository: MDBMoviesRepository
) : NetworkBoundResource<MDBMovieModel, UseCaseGetMovie.Params>() {

    override suspend fun saveCallResult(item: MDBMovieModel) {
        repository.saveMovie(item)
    }

    override fun shouldLoad(params: Params, data: MDBMovieModel): Boolean = true

    override suspend fun loadFromDb(params: Params): MDBMovieModel =
        repository.loadMDBMovieDetail(params.movieId)

    override suspend fun createCall(params: Params): MDBMovieModel =
        repository.getMDBMovieDetail(params.movieId, params.language)

    override fun getLoadingObject(): MDBMovieModel = MDBMovieModel()

    data class Params constructor(
        val movieId: Int,
        val language: String = HttpBaseValues.LANGUAGE
    )
}
