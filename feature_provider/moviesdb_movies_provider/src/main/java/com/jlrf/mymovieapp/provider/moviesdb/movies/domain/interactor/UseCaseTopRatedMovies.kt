package com.jlrf.mymovieapp.provider.moviesdb.movies.domain.interactor

import com.jlrf.mymovieapp.provider.moviesdb.movies.domain.MDBMoviesRepository
import com.jlrf.mymovieapp.provider.moviesdb.movies.domain.interactor.base.NetworkBoundResource
import com.jlrf.mymovieapp.provider.moviesdb.movies.domain.model.MDBMovieListModel
import com.jlrf.network.factory.custom_callback.HttpBaseValues
import javax.inject.Inject

class UseCaseTopRatedMovies @Inject constructor(
    private val repository: MDBMoviesRepository
) : NetworkBoundResource<MDBMovieListModel, UseCaseTopRatedMovies.Params>() {

    data class Params constructor(
        val language: String = HttpBaseValues.LANGUAGE,
        val page: Int = HttpBaseValues.PAGE
    )

    override suspend fun saveCallResult(item: MDBMovieListModel) {
        repository.saveTopRatedMovies(item.results)
    }

    override fun shouldLoad(params: Params, data: MDBMovieListModel): Boolean = false

    override suspend fun loadFromDb(params: Params): MDBMovieListModel =
        repository.loadTopRatedMovies()

    override suspend fun createCall(params: Params): MDBMovieListModel =
        repository.getTopRated(params.page, params.language)

    override fun getLoadingObject(): MDBMovieListModel = MDBMovieListModel()
}
