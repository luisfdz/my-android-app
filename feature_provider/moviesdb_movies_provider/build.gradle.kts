plugins {
    id(Plugins.jacocoMerged)
    id("com.android.library")
    kotlin("android")
    kotlin("kapt")
    kotlin("android.extensions")
}
android {
    compileSdkVersion(Versions.compileSdk)
    buildToolsVersion(Versions.buildTools)
    defaultConfig {
        minSdkVersion(Android.minSdkVersion)
        targetSdkVersion(Android.targetSdkVersion)
        versionCode = Android.versionCode
        versionName = Android.versionName
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android.txt"), "proguard-rules.pro")
        }
        getByName("debug") {
            isMinifyEnabled = false
            isTestCoverageEnabled = true
        }
    }

    testOptions {
        animationsDisabled = true
        unitTests.apply {
            isReturnDefaultValues = true
            isIncludeAndroidResources = true
            all(KotlinClosure1<Any, Test>({
                (this as Test).also {
                    reports.junitXml.destination =
                        file("${parent?.buildDir}/reports/${project.name}/test/")
                    reports.html.destination =
                        file("${parent?.buildDir}/reports/${project.name}/test/")
                }
            }, this))
        }
        execution = "ANDROIDX_TEST_ORCHESTRATOR"
    }

    lintOptions {
        htmlReport = true
        xmlReport = true
        isAbortOnError = true
        setHtmlOutput(file("${parent?.buildDir}/reports/${project.name}/linter/lint-results.html"))
        setXmlOutput(file("${parent?.buildDir}/reports/${project.name}/linter/lint-results.xml"))
    }
}

dependencies {
    implementation(fileTree("dir" to "libs", "include" to listOf("*.jar")))
    api(Libs.kotlin)
    api(Libs.kotlinReflect)
    api(Libs.daggerAndroid)
    api(Libs.daggerAndroidSupport)
    api(project(Project.base_provider))

    kapt(Libs.roomCompiler)
    kapt(Libs.lifeCycleCompiler)
    kapt(Libs.daggerCompiler)
    kapt(Libs.daggerAndroidProcessor)

    testImplementation(TestLibs.jUnit)
    testImplementation(project(":base_tests"))
    testImplementation(TestLibs.roomTesting)
    testImplementation(TestLibs.mockk)
    testImplementation(TestLibs.coroutines)

    androidTestImplementation(TestLibs.mockkAndroid)
    androidTestImplementation(TestLibs.testRunner)
    androidTestImplementation(TestLibs.testExt)
    androidTestImplementation(TestLibs.espressoCore)
    androidTestUtil(TestLibs.testOrchestrator)
}
