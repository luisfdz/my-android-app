plugins {
    jacoco
}

val jacocoTestReport by tasks.registering(JacocoReport::class) {
    dependsOn("testDebugUnitTest")
    reports {
        xml.isEnabled = false
        csv.isEnabled = false
        html.isEnabled = true
        html.destination = file("${parent?.buildDir}/reports/${project.name}/jacoco/")
    }

    val excludes = listOf(
        "**/*\$run$1.class",
        "**/R.class",
        "**/R$*.class",
        "**/*\$ViewInjector*.*",
        "**/BuildConfig.*",
        "**/Manifest*.*",
        "**/*Test*.*",
        "android/**/*.*",
        "**/*Fragment.*",
        "**/*Activity.*",
        "**/*_MembersInjector.class",
        "**/Dagger*Component*.class",
        "**/Dagger*Subcomponent*.class",
        "**/*Subcomponent\$Builder.class",
        "**/*Module_*Factory.class",
        "**/*_MembersInjector*.*",
        "**/*_*Factory*.*",
        "**/androidTest/**",
        "**/*Component*.*",
        "**/*Module*.*",
        "$buildDir/generated/source/kapt/**"
    )
    classDirectories.setFrom(
        fileTree("dir" to "$buildDir/tmp/kotlin-classes/debug", "excludes" to excludes)
            .plus(
                fileTree(
                    "dir" to "$buildDir/intermediates/classes/debug",
                    "excludes" to excludes
                )
            )
            .plus(fileTree("build/classes/kotlin/main"))
            .plus(
                fileTree(
                    "dir" to "$buildDir/intermediates/classes/debug/com",
                    "excludes" to excludes
                )
            )
            .plus(
                fileTree(
                    "dir" to "$buildDir/tmp/kotlin-classes/debug/com",
                    "excludes" to excludes
                )
            )
    )
    executionData.setFrom(
        fileTree(
            "dir" to project.buildDir,
            "include" to listOf("**/*.exec", "**/*.ec")
        )
    )

    sourceDirectories.setFrom("${project.projectDir}/src/main/java")

}

val jacocoTestCoverageVerification by tasks.registering(JacocoCoverageVerification::class) {
    violationRules {
        rule {
            limit {
                minimum = "0.5".toBigDecimal()
            }
        }

        rule {
            enabled = false
            element = "CLASS"
            includes = listOf("org.gradle.*")

            limit {
                counter = "LINE"
                value = "TOTALCOUNT"
                maximum = "0.3".toBigDecimal()
            }
        }
    }
}


jacoco {
    toolVersion = Versions.jacoco
    reportsDir = file("${parent?.buildDir}/reports/${project.name}/jacoco")
}


