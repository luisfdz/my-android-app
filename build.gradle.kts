import org.gradle.api.tasks.testing.logging.TestExceptionFormat
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import org.jmailen.gradle.kotlinter.tasks.FormatTask
import org.jmailen.gradle.kotlinter.tasks.LintTask

plugins {
    application
    jacoco
}
buildscript {
    repositories {
        google()
        jcenter()
        mavenCentral()
        maven("https://plugins.gradle.org/m2/")
    }

    dependencies {
        classpath(BuildPlugins.android)
        classpath(BuildPlugins.kotlin)
        classpath(BuildPlugins.kotlinExt)
        classpath(BuildPlugins.navigation)
        classpath(BuildPlugins.klint)
        classpath(BuildPlugins.jacoco)
    }
}

allprojects {
    repositories {
        google()
        jcenter()
        mavenCentral()
        maven("https://plugins.gradle.org/m2/")
    }
}

subprojects {
    apply {
        plugin(Plugins.klint)
    }

    val ktLint by tasks.creating(LintTask::class) {
        group = "verification"
        source(files("src"))
        reports = mapOf(
            "plain" to file("${parent?.buildDir}/reports/${project.name}/linter/lint-report.txt"),
            "json" to file("${parent?.buildDir}/reports/${project.name}/linter/lint-report.json"),
            "html" to file("${parent?.buildDir}/reports/${project.name}/linter/lint-report.html")
        )
    }

    val ktFormat by tasks.creating(FormatTask::class) {
        group = "formatting"
        source(files("src"))
        report = file("${parent?.buildDir}/reports/${project.name}/format/format-report.txt")
    }
}
evaluationDependsOnChildren()

tasks.withType<Test> {
    configure<JacocoTaskExtension> {
        isIncludeNoLocationClasses = true
    }
    useJUnitPlatform { }
    testLogging {
        events("passed", "skipped", "failed")
        exceptionFormat = TestExceptionFormat.FULL
    }
}

tasks.withType<Test>().all {
    jvmArgs("-noverify")
}
tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}

tasks.withType<JavaExec>()

val clean = tasks.getByName("clean") {
    delete(fileTree(project.buildDir) {
        include("*.apk")
    })
}

fun taskName(
    taskName: String = "",
    taskNames: MutableList<String> = mutableListOf()
): String = if (taskNames.isEmpty()) {
    taskNames.add(taskName)
    taskName
} else {
    var name = ""
    taskNames.forEach {
        name = if (name.isEmpty()) {
            it
        } else {
            "_$it"
        }
    }
    name
}

fun runModuleTask(
    module: String,
    taskName: String = "",
    taskNames: MutableList<String> = mutableListOf()
): Task {
    return tasks.create("${module}_${taskName(taskName, taskNames)}") {
        val project = subprojects.asSequence().filter { it.name == module }.first()
        project.childProjects.forEach { (_, module) ->
            taskNames.forEach { taskName ->
                val task = module.getTasksByName(taskName, true)
                this.dependsOn(task)
            }
        }
    }
}

fun runAppTask(taskName: String = "", taskNames: MutableList<String> = mutableListOf()): Task {
    if (taskNames.isEmpty()) {
        taskNames.add(taskName)
    }
    return tasks.create("app_${taskName(taskName, taskNames)}") {
        taskNames.forEach { taskName ->
            this.dependsOn(":app:$taskName")
        }
    }
}

tasks.create("projectClean") {
    group = "cleanup"
    description = "Runs all clean on all project modules"
    dependsOn(
        runAppTask(taskName = "clean")
            .dependsOn(
                runModuleTask(
                    module = "feature_provider",
                    taskName = "clean"
                )
                    .dependsOn(
                        runModuleTask(
                            module = "core",
                            taskName = "clean"
                        )
                    )
            )
    )
}

tasks.create("projectFormat") {
    group = "formatting"
    description = "Runs Code format on all project modules"
    dependsOn(
        runAppTask(taskName = "ktFormat")
            .dependsOn(
                runModuleTask(
                    module = "feature_provider",
                    taskName = "ktFormat"
                )
                    .dependsOn(
                        runModuleTask(
                            module = "core",
                            taskName = "ktFormat"
                        )
                    )
            )
    )
}

tasks.create("projectLinter") {
    group = "verification"
    description = "Runs Code Lint Checker on all project modules"
    dependsOn(
        runAppTask(taskNames = mutableListOf("lint", "ktLint"))
            .dependsOn(
                runModuleTask(
                    module = "feature_provider",
                    taskNames = mutableListOf("lint", "ktLint")
                )
                    .dependsOn(
                        runModuleTask(
                            module = "core",
                            taskNames = mutableListOf("lint", "ktLint")
                        )
                    )
            )
    )
}

tasks.create("projectUnitTest") {
    group = "verification"
    description = "Runs Unit Test on all project modules"
    dependsOn(
        runAppTask(taskName = "testDebugUnitTest")
            .dependsOn(
                runModuleTask(
                    module = "feature_provider",
                    taskName = "testDebugUnitTest"
                )
                    .dependsOn(
                        runModuleTask(
                            module = "core",
                            taskName = "testDebugUnitTest"
                        )
                    )
            )
    )
}

tasks.create("projectJacocoReport") {
    group = "verification"
    description = "Runs Jacoco Test Report on all project modules"
    dependsOn(
        runAppTask(taskName = "jacocoTestReport")
            .dependsOn(
                runModuleTask(
                    module = "feature_provider",
                    taskName = "jacocoTestReport"
                )
                    .dependsOn(
                        runModuleTask(
                            module = "core",
                            taskName = "jacocoTestReport"
                        )
                    )
            )
    )
}

tasks.create("projectTestCoverageVerification") {
    group = "verification"
    description = "Runs Jacoco Test Report on all project modules"
    dependsOn(
        runAppTask(taskName = "jacocoTestCoverageVerification")
            .dependsOn(
                runModuleTask(
                    module = "feature_provider",
                    taskName = "jacocoTestCoverageVerification"
                )
                    .dependsOn(
                        runModuleTask(
                            module = "core",
                            taskName = "jacocoTestCoverageVerification"
                        )
                    )
            )
    )
}
