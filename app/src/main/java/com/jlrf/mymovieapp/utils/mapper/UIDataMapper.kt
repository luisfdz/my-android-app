package com.jlrf.mymovieapp.utils.mapper

import com.jlrf.mymovieapp.movie_detail.MovieDetailDataUIModel
import com.jlrf.mymovieapp.provider.moviesdb.movies.domain.model.MDBDateModel
import com.jlrf.mymovieapp.provider.moviesdb.movies.domain.model.MDBGenreModel
import com.jlrf.mymovieapp.provider.moviesdb.movies.domain.model.MDBMovieListModel
import com.jlrf.mymovieapp.provider.moviesdb.movies.domain.model.MDBMovieModel
import com.jlrf.mymovieapp.utils.mapper.models.DatesUIModel
import com.jlrf.mymovieapp.utils.mapper.models.MovieUIModel
import com.jlrf.mymovieapp.utils.mapper.models.MovieUIModelList
import javax.inject.Inject

class UIDataMapper @Inject constructor() {

    fun convert(model: MDBMovieModel): MovieDetailDataUIModel {
        val uiModel = MovieDetailDataUIModel(
            name = model.title,
            posterUrl = model.posterPath,
            bannerUrl = model.backdropPath,
            description = model.overview,
            genres = convertGenreToString(model.genre),
            viewers = model.popularity,
            cast = arrayListOf(),
            rating = model.voteAverage
        )
        uiModel.state = model.status
        return uiModel
    }

    private fun convertGenreToString(genres: List<MDBGenreModel>): String {
        var genreString = String()
        genres.map {
            genreString = "$genreString ${it.name}"
        }
        return genreString
    }

    private fun convertToLegacyModel(model: MDBMovieModel): MovieUIModel {
        val uiModel = MovieUIModel(
            id = model.id,
            adult = model.isAdult,
            backdropPath = model.backdropPath,
            budget = model.budget,
            genreIds = model.genreIds,
            homepage = model.homePage,
            imdbId = model.imdbId,
            originalLanguage = model.originalLanguage,
            originalTitle = model.originalTitle,
            overview = model.overview,
            popularity = model.popularity,
            posterPath = model.posterPath,
            revenue = model.revenue,
            runtime = model.runTime,
            spokenLanguageModels = model.spokenLanguages.map { it.name },
            status = model.status,
            tagline = model.tagline,
            title = model.title,
            video = model.video,
            voteAverage = model.voteAverage,
            voteCount = model.voteCount,
            releaseDate = model.releaseDate.toString()
        )
        uiModel.state = model.status
        return uiModel
    }

    fun convert(model: MDBDateModel): DatesUIModel {
        val uiModel = DatesUIModel(
            maximum = model.maximum,
            minimum = model.minimum
        )
        uiModel.state = model.status
        return uiModel
    }

    fun convert(model: MDBMovieListModel): MovieUIModelList {
        val uiModel = MovieUIModelList(
            dates = convert(model.movieDates),
            page = model.page,
            results = model.results.map { convertToLegacyModel(it) },
            totalPages = model.totalPages,
            totalResult = model.totalResults
        )
        uiModel.state = model.status
        return uiModel
    }
}
