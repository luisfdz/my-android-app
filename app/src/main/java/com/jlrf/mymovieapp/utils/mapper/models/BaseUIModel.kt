package com.jlrf.mymovieapp.utils.mapper.models

import android.os.Parcelable
import androidx.annotation.StringRes
import kotlinx.android.parcel.Parcelize

@Parcelize
open class BaseUIModel(
    var error: Boolean = EMPTY_BOOLEAN,
    var state: String = LOADING,
    @StringRes var messageId: Int = EMPTY_INT,
    var errorCode: Int = EMPTY_INT
) : Parcelable {

    companion object {
        const val SUCCESS = "SUCCESS"
        const val LOADING = "LOADING"
        const val ERROR = "ERROR"
        const val EMPTY_STRING = ""
        const val EMPTY_INT = 0
        const val EMPTY_DOUBLE = 0.0
        const val EMPTY_BOOLEAN = false
    }
}

/**
 * Shallow copy of base class [BaseUIModel]
 *
 * @param [BaseUIModel] base class
 * @return [T] sub class
 */
fun <T : BaseUIModel> T.baseCopy(base: BaseUIModel): T {
    state = base.state
    error = base.error
    errorCode = base.errorCode
    return this
}
