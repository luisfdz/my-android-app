package com.jlrf.mymovieapp.utils.mapper.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class MovieUIModel(
    val id: Int = EMPTY_INT,
    val adult: Boolean = EMPTY_BOOLEAN,
    val backdropPath: String = EMPTY_STRING,
    val budget: Int = EMPTY_INT,
    val genreIds: List<Int>,
    val homepage: String = EMPTY_STRING,
    val imdbId: String = EMPTY_STRING,
    val originalLanguage: String = EMPTY_STRING,
    val originalTitle: String = EMPTY_STRING,
    val overview: String = EMPTY_STRING,
    val popularity: Double = EMPTY_DOUBLE,
    val posterPath: String = EMPTY_STRING,
    val releaseDate: String = EMPTY_STRING,
    val revenue: Int = EMPTY_INT,
    val runtime: Int = EMPTY_INT,
    val spokenLanguageModels: List<String>,
    val status: String = EMPTY_STRING,
    val tagline: String = EMPTY_STRING,
    val title: String = EMPTY_STRING,
    val video: Boolean = EMPTY_BOOLEAN,
    val voteAverage: Double = EMPTY_DOUBLE,
    val voteCount: Int = EMPTY_INT
) : BaseUIModel(), Parcelable

@Parcelize
data class MovieUIModelList(
    val dates: DatesUIModel? = null,
    val page: Int = EMPTY_INT,
    val results: List<MovieUIModel> = arrayListOf(),
    val totalPages: Int = EMPTY_INT,
    val totalResult: Int = EMPTY_INT
) : BaseUIModel(), Parcelable

@Parcelize
data class DatesUIModel(
    val maximum: String = EMPTY_STRING,
    val minimum: String = EMPTY_STRING
) : BaseUIModel()
