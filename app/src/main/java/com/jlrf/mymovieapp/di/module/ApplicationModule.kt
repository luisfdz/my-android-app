package com.jlrf.mymovieapp.di.module

import android.app.Application
import android.content.Context
import android.content.res.Resources
import com.jlrf.storage.utils.DatabaseConstants.DATABASE_NAME
import com.jlrf.storage.utils.DatabaseConstants.DATABASE_NAME_PROPERTY
import dagger.Module
import dagger.Provides
import javax.inject.Named

@Module
class ApplicationModule {
    @Provides
    fun providesApplicationContext(application: Application): Context =
        application.applicationContext

    @Provides
    fun providesResources(application: Application): Resources = application.resources

    @Provides
    @Named(DATABASE_NAME_PROPERTY)
    fun provideDatabaseName(): String = DATABASE_NAME
}
