package com.jlrf.mymovieapp.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.jlrf.mymovieapp.di.scope.ViewModelScope
import com.jlrf.mymovieapp.movie_detail.MovieDetailViewModel
import com.jlrf.mymovieapp.movies.MovieListViewModel
import com.jlrf.mymovieapp.provider.moviesdb.movies.di.MDBMoviesProviderModule
import com.jlrf.mymovieapp.utils.ViewModelFactoryProvider
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module(includes = [MDBMoviesProviderModule::class])
abstract class MDBFeatureModule {
    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactoryProvider): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelScope(MovieListViewModel::class)
    abstract fun bindMoviesViewModel(viewModel: MovieListViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelScope(MovieDetailViewModel::class)
    abstract fun bindMovieDetailViewModel(viewModel: MovieDetailViewModel): ViewModel
}
