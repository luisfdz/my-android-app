package com.jlrf.mymovieapp.di.component

import android.app.Application
import android.content.Context
import com.jlrf.mymovieapp.MyMovieApp
import com.jlrf.mymovieapp.di.module.AppBindingModule
import com.jlrf.mymovieapp.di.module.ApplicationModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        ApplicationModule::class,
        AndroidInjectionModule::class,
        AppBindingModule::class
    ]
)
interface ApplicationComponent : AndroidInjector<MyMovieApp> {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): ApplicationComponent
    }

    fun context(): Context
}
