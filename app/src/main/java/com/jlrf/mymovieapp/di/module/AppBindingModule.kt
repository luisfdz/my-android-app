package com.jlrf.mymovieapp.di.module

import com.jlrf.mymovieapp.movie_detail.MovieDetailFragment
import com.jlrf.mymovieapp.movies.MovieListFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class AppBindingModule {

    @ContributesAndroidInjector(modules = [MDBFeatureModule::class])
    abstract fun movieListFragment(): MovieListFragment

    @ContributesAndroidInjector(modules = [MDBFeatureModule::class])
    abstract fun movieDetailFragment(): MovieDetailFragment
}
