package com.jlrf.mymovieapp.movie_detail

import android.os.Parcelable
import com.jlrf.mymovieapp.utils.mapper.models.BaseUIModel
import kotlinx.android.parcel.Parcelize

@Parcelize
data class MovieDetailDataUIModel(
    val name: String = EMPTY_STRING,
    val posterUrl: String = EMPTY_STRING,
    val bannerUrl: String = EMPTY_STRING,
    val description: String = EMPTY_STRING,
    val genres: String = EMPTY_STRING,
    val viewers: Double = EMPTY_DOUBLE,
    val cast: List<MovieDetailCastModel> = arrayListOf(),
    val rating: Double = EMPTY_DOUBLE
) : BaseUIModel(), Parcelable

@Parcelize
data class MovieDetailCastModel(
    val name: String,
    val pictureUrl: String,
    val role: String
) : BaseUIModel(), Parcelable
