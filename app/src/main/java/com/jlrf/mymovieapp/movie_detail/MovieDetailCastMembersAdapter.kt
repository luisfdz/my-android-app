package com.jlrf.mymovieapp.movie_detail

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jlrf.mymovieapp.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.cast_member_item_viewholder.view.imageViewCastPic
import kotlinx.android.synthetic.main.cast_member_item_viewholder.view.textViewCastMemberName
import kotlinx.android.synthetic.main.cast_member_item_viewholder.view.textViewCastMemberRole

class MovieDetailCastMembersAdapter(private val movieDetailUI: MovieDetailDataUIModel) :
    RecyclerView.Adapter<CastMemberViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CastMemberViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.cast_member_item_viewholder, parent, false)
        return CastMemberViewHolder(view)
    }

    override fun getItemCount(): Int = movieDetailUI.cast.size

    override fun onBindViewHolder(holder: CastMemberViewHolder, position: Int) {
        val castMember = movieDetailUI.cast[position]
        holder.itemView.textViewCastMemberName.text = castMember.name
        holder.itemView.textViewCastMemberRole.text = castMember.role
        Picasso.get().load(castMember.pictureUrl).placeholder(R.mipmap.ic_launcher)
            .fit()
            .into(holder.itemView.imageViewCastPic)
    }
}

class CastMemberViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
