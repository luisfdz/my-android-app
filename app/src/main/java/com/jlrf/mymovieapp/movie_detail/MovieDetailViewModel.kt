package com.jlrf.mymovieapp.movie_detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.jlrf.mymovieapp.provider.moviesdb.movies.domain.interactor.UseCaseGetMovie
import com.jlrf.mymovieapp.utils.mapper.UIDataMapper
import javax.inject.Inject
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class MovieDetailViewModel @Inject constructor(
    private val useCaseGetMovie: UseCaseGetMovie,
    private val mapper: UIDataMapper
) : ViewModel() {

    private val movieUI: MediatorLiveData<MovieDetailDataUIModel> = MediatorLiveData()

    fun getMovieDetail(id: Int) {
        viewModelScope.launch {
            useCaseGetMovie.execute(UseCaseGetMovie.Params(id)).collect {
                movieUI.postValue(mapper.convert(it))
            }
        }
    }

    fun movieDetail(): LiveData<MovieDetailDataUIModel> = movieUI
}
