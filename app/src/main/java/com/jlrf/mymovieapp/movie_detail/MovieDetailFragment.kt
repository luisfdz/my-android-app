package com.jlrf.mymovieapp.movie_detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.jlrf.mymovieapp.R
import com.jlrf.mymovieapp.utils.mapper.models.BaseUIModel.Companion.SUCCESS
import com.squareup.picasso.Picasso
import dagger.android.support.DaggerFragment
import javax.inject.Inject
import kotlinx.android.synthetic.main.movie_detail_fragment.view.imageViewBanner
import kotlinx.android.synthetic.main.movie_detail_fragment.view.imageViewPoster
import kotlinx.android.synthetic.main.movie_detail_fragment.view.ratingBar
import kotlinx.android.synthetic.main.movie_detail_fragment.view.recyclerViewCastMembers
import kotlinx.android.synthetic.main.movie_detail_fragment.view.textViewGenres
import kotlinx.android.synthetic.main.movie_detail_fragment.view.textViewMovieDescription
import kotlinx.android.synthetic.main.movie_detail_fragment.view.textViewMovieTitle
import kotlinx.android.synthetic.main.movie_detail_fragment.view.textViewRatingLabel
import kotlinx.android.synthetic.main.movie_detail_fragment.view.textViewViewers

class MovieDetailFragment : DaggerFragment() {

    @Inject
    lateinit var factory: ViewModelProvider.Factory

    private val args: MovieDetailFragmentArgs by navArgs()

    private val movieId by lazy { args.movieId }

    private val viewModel by lazy {
        ViewModelProvider(this, factory).get(MovieDetailViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.movie_detail_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.movieDetail().observe(viewLifecycleOwner, Observer {
            when (it.state) {
                SUCCESS -> {
                    bindItem(it)
                }
            }
        }
        )
        viewModel.getMovieDetail(movieId)
    }

    private fun bindItem(movieUI: MovieDetailDataUIModel) {
        val view = requireView()
        val castMemberAdapter = MovieDetailCastMembersAdapter(movieUI)
        view.textViewMovieDescription.text = movieUI.description
        movieUI.genres.takeIf { it.isNotEmpty() }?.let {
            view.textViewGenres.text = it
            view.textViewGenres.visibility = View.VISIBLE
        }
        view.ratingBar.rating = movieUI.rating.toFloat()
        view.textViewRatingLabel.text = movieUI.rating.toString()
        Picasso.get().isLoggingEnabled = true
        Picasso.get().load(movieUI.bannerUrl).fit().placeholder(R.mipmap.ic_launcher)
            .into(view.imageViewBanner)
        Picasso.get().load(movieUI.posterUrl).fit().placeholder(R.mipmap.ic_launcher)
            .into(view.imageViewPoster)
        val viewersText =
            view.context.getString(R.string.movie_detail_people_watching, movieUI.viewers.toInt())
        view.textViewViewers.text = viewersText
        view.recyclerViewCastMembers.layoutManager =
            LinearLayoutManager(requireContext(), RecyclerView.HORIZONTAL, false)
        view.recyclerViewCastMembers.adapter = castMemberAdapter
        view.textViewMovieTitle.text = movieUI.name
    }
}
