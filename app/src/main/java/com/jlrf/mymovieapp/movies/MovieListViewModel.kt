package com.jlrf.mymovieapp.movies

import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.jlrf.mymovieapp.provider.moviesdb.movies.domain.interactor.UseCaseNowPlayingMovies
import com.jlrf.mymovieapp.provider.moviesdb.movies.domain.interactor.UseCasePopularMovies
import com.jlrf.mymovieapp.provider.moviesdb.movies.domain.interactor.UseCaseTopRatedMovies
import com.jlrf.mymovieapp.provider.moviesdb.movies.domain.interactor.UseCaseUpcomingMovies
import com.jlrf.mymovieapp.utils.mapper.UIDataMapper
import com.jlrf.mymovieapp.utils.mapper.models.MovieUIModelList
import javax.inject.Inject
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class MovieListViewModel @Inject constructor(
    private val useCasePopularMovies: UseCasePopularMovies,
    private val useCaseNowPlayingMovies: UseCaseNowPlayingMovies,
    private val useCaseTopRatedMovies: UseCaseTopRatedMovies,
    private val useCaseUpcomingMovies: UseCaseUpcomingMovies,
    private val mapper: UIDataMapper
) : ViewModel() {

    val popularMoviesList: MediatorLiveData<MovieUIModelList> = MediatorLiveData()
    val nowPlayingMoviesList: MediatorLiveData<MovieUIModelList> = MediatorLiveData()
    val topRatedMovies: MediatorLiveData<MovieUIModelList> = MediatorLiveData()
    val upcomingMovies: MediatorLiveData<MovieUIModelList> = MediatorLiveData()

    fun getNowPlayingMovies() {
        viewModelScope.launch {
            useCaseNowPlayingMovies.execute(UseCaseNowPlayingMovies.Params()).collect {
                nowPlayingMoviesList.postValue(mapper.convert(it))
            }
        }
    }

    fun getTopRatedMovies() {
        viewModelScope.launch {
            useCaseTopRatedMovies.execute(UseCaseTopRatedMovies.Params()).collect {
                topRatedMovies.postValue(mapper.convert(it))
            }
        }
    }

    fun getUpcomingMovies() {
        viewModelScope.launch {
            useCaseUpcomingMovies.execute(UseCaseUpcomingMovies.Params()).collect {
                upcomingMovies.postValue(mapper.convert(it))
            }
        }
    }

    fun getPopularMovies() {
        viewModelScope.launch {
            useCasePopularMovies.execute(UseCasePopularMovies.Params()).collect {
                popularMoviesList.postValue(mapper.convert(it))
            }
        }
    }
}
