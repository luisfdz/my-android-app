package com.jlrf.mymovieapp.movies

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jlrf.mymovieapp.R
import com.jlrf.mymovieapp.utils.mapper.models.MovieUIModel
import com.jlrf.mymovieapp.utils.mapper.models.MovieUIModelList
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.movie_item_viewholder.view.imageViewMovie
import kotlinx.android.synthetic.main.movie_item_viewholder.view.textViewMovieName

class MovieListCategoryAdapter(
    var moviesUiModel: MovieUIModelList? = null,
    private val listener: (MovieUIModel) -> Unit
) : RecyclerView.Adapter<MovieListViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieListViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.movie_item_viewholder, parent, false)
        return MovieListViewHolder(view)
    }

    override fun getItemCount(): Int = moviesUiModel?.results?.size ?: 0

    override fun onBindViewHolder(holder: MovieListViewHolder, position: Int) {
        moviesUiModel?.let {
            val movie = it.results[position]
            holder.itemView.textViewMovieName.text = movie.title
            if (movie.posterPath.isNotEmpty()) {
                Picasso.get().load(movie.posterPath).placeholder(R.mipmap.ic_launcher)
                    .fit()
                    .into(holder.itemView.imageViewMovie)
            }
            holder.itemView.setOnClickListener {
                listener.invoke(movie)
            }
        }
    }
}

class MovieListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
