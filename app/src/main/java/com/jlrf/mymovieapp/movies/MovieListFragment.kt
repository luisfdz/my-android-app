package com.jlrf.mymovieapp.movies

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.jlrf.mymovieapp.R
import com.jlrf.mymovieapp.utils.mapper.models.BaseUIModel
import com.jlrf.mymovieapp.utils.mapper.models.MovieUIModelList
import dagger.android.support.DaggerFragment
import javax.inject.Inject
import kotlinx.android.synthetic.main.movie_list_fragment.progressBar
import kotlinx.android.synthetic.main.movie_list_fragment.recycerViewMovieList

class MovieListFragment : DaggerFragment() {

    @Inject
    lateinit var factory: ViewModelProvider.Factory
    lateinit var adapter: MovieListAdapter
    private val viewModel by lazy {
        ViewModelProvider(this, factory).get(MovieListViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.movie_list_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        adapter = MovieListAdapter(listener = {
            findNavController().navigate(
                MovieListFragmentDirections.actionMoviesListFragmentToMovieDetail(it.id)
            )
        })
        recycerViewMovieList.adapter = adapter
        initializeViewModels()
        viewModel.getPopularMovies()
        viewModel.getTopRatedMovies()
        viewModel.getNowPlayingMovies()
        viewModel.getUpcomingMovies()
    }

    private fun initializeViewModels() {
        viewModel.popularMoviesList.observe(viewLifecycleOwner, Observer {
            when (it.state) {
                BaseUIModel.SUCCESS -> {
                    addCategoryToAdapter(it, getString(R.string.popular_category))
                    viewModel.popularMoviesList.removeObservers(viewLifecycleOwner)
                }
            }
        })
        viewModel.nowPlayingMoviesList.observe(viewLifecycleOwner, Observer {
            when (it.state) {
                BaseUIModel.SUCCESS -> {
                    addCategoryToAdapter(it, getString(R.string.now_playing_category))
                    viewModel.nowPlayingMoviesList.removeObservers(viewLifecycleOwner)
                }
            }
        })
        viewModel.upcomingMovies.observe(viewLifecycleOwner, Observer {
            when (it.state) {
                BaseUIModel.SUCCESS -> {
                    addCategoryToAdapter(it, getString(R.string.upcoming_category))
                    viewModel.upcomingMovies.removeObservers(viewLifecycleOwner)
                }
            }
        })
        viewModel.topRatedMovies.observe(viewLifecycleOwner, Observer {
            when (it.state) {
                BaseUIModel.SUCCESS -> {
                    addCategoryToAdapter(it, getString(R.string.top_rated_category))
                    viewModel.topRatedMovies.removeObservers(viewLifecycleOwner)
                }
            }
        })
    }

    private fun addCategoryToAdapter(result: MovieUIModelList, category: String) {
        adapter.addCategory(result, category)
        progressBar.visibility = View.GONE
        recycerViewMovieList.visibility = View.VISIBLE
    }
}
