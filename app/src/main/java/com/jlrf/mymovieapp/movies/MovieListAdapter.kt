package com.jlrf.mymovieapp.movies

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.jlrf.mymovieapp.R
import com.jlrf.mymovieapp.utils.mapper.models.MovieUIModel
import com.jlrf.mymovieapp.utils.mapper.models.MovieUIModelList
import kotlinx.android.synthetic.main.movie_list_category_viewholder.view.recyclerViewMovies
import kotlinx.android.synthetic.main.movie_list_category_viewholder.view.textViewCategoryName

class MovieListAdapter(private val listener: (MovieUIModel) -> Unit) : RecyclerView.Adapter<MoviewlistAdapterViewHolder>() {

    private val moviesLists = ArrayList<MovieUIModelList>()
    private val movieNames = ArrayList<String>()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MoviewlistAdapterViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.movie_list_category_viewholder, parent, false)
        return MoviewlistAdapterViewHolder(view)
    }

    override fun getItemCount(): Int = moviesLists.size

    override fun onBindViewHolder(holder: MoviewlistAdapterViewHolder, position: Int) {
        holder.itemView.recyclerViewMovies.layoutManager = LinearLayoutManager(holder.itemView.context,
        RecyclerView.HORIZONTAL, false)
        holder.itemView.textViewCategoryName.text = movieNames[position]
        val adapter = MovieListCategoryAdapter(moviesLists[position], listener)
        holder.itemView.recyclerViewMovies.adapter = adapter
    }

    fun addCategory(model: MovieUIModelList, category: String) {
        synchronized(this) {
            moviesLists.add(model)
            val index = moviesLists.indexOf(model)
            movieNames.add(index, category)
            notifyItemChanged(index)
        }
    }

    fun clearCategories() {
        moviesLists.clear()
        movieNames.clear()
        notifyDataSetChanged()
    }
}

class MoviewlistAdapterViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
