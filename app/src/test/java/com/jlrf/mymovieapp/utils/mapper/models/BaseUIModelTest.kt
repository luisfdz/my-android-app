package com.jlrf.mymovieapp.utils.mapper.models

import org.junit.Test

internal class BaseUIModelTest {

    @Test
    fun creationOfBaseUIModel() {

        val model = BaseUIModel()

        assert(model.error == BaseUIModel.EMPTY_BOOLEAN)
        assert(model.state == BaseUIModel.LOADING)
        assert(model.messageId == BaseUIModel.EMPTY_INT)
        assert(model.errorCode == BaseUIModel.EMPTY_INT)
    }

    @Test
    fun createBaseUiModelAndCopy() {

        val model = BaseUIModel()

        val copy = BaseUIModel(state = BaseUIModel.ERROR)

        copy.baseCopy(model)

        assert(model.error == copy.error)
        assert(model.state == copy.state)
        assert(model.messageId == copy.messageId)
        assert(model.errorCode == copy.errorCode)
    }
}
