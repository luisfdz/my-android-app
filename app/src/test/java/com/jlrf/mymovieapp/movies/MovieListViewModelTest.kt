package com.jlrf.mymovieapp.movies

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.jlrf.base_provider.BaseModel
import com.jlrf.mymovieapp.extensions.getOrAwaitValue
import com.jlrf.mymovieapp.provider.moviesdb.movies.domain.interactor.UseCaseNowPlayingMovies
import com.jlrf.mymovieapp.provider.moviesdb.movies.domain.interactor.UseCasePopularMovies
import com.jlrf.mymovieapp.provider.moviesdb.movies.domain.interactor.UseCaseTopRatedMovies
import com.jlrf.mymovieapp.provider.moviesdb.movies.domain.interactor.UseCaseUpcomingMovies
import com.jlrf.mymovieapp.provider.moviesdb.movies.domain.model.MDBMovieListModel
import com.jlrf.mymovieapp.provider.moviesdb.movies.domain.model.MDBMovieModel
import com.jlrf.mymovieapp.utils.mapper.UIDataMapper
import io.mockk.every
import io.mockk.mockk
import io.mockk.spyk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.newSingleThreadContext
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Ignore
import org.junit.Rule
import org.junit.Test

internal class MovieListViewModelTest {

    private val useCasePopularMovies: UseCasePopularMovies = mockk()
    private val useCaseNowPlayingMovies: UseCaseNowPlayingMovies = mockk()
    private val useCaseTopRatedMovies: UseCaseTopRatedMovies = mockk()
    private val useCaseUpcomingMovies: UseCaseUpcomingMovies = mockk()
    private val mapper: UIDataMapper = UIDataMapper()
    private val mainThreadSurrogate = newSingleThreadContext("UI thread")

    private lateinit var viewModel: MovieListViewModel

    @Rule
    @JvmField
    var instantExecutorRule: InstantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setUp() {
        viewModel = spyk(MovieListViewModel(useCasePopularMovies, useCaseNowPlayingMovies, useCaseTopRatedMovies, useCaseUpcomingMovies, mapper), recordPrivateCalls = true)
        Dispatchers.setMain(mainThreadSurrogate)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain() // reset main dispatcher to the original Main dispatcher
        mainThreadSurrogate.close()
    }

    @ExperimentalCoroutinesApi
    @Test
    fun getPopularMoviesWithError() {
        runBlockingTest {
            val flow = flow {
                emit(MDBMovieListModel().apply {
                    setError(BaseModel.COMMON_ERROR)
                })
            }
            every { useCasePopularMovies.execute(any()) } returns flow
            viewModel.popularMoviesList.observeForever {}

            viewModel.getPopularMovies()
            assert(viewModel.popularMoviesList.hasObservers())
            viewModel.popularMoviesList.value?.error?.let { assert(it) }
            viewModel.popularMoviesList.value?.errorCode?.let { assert(BaseModel.COMMON_ERROR == it) }
        }
    }

    @ExperimentalCoroutinesApi
    @Ignore("Temporary ignored to be able to generate Gitlab Page")
    @Test
    fun getPopularMovies() {
        runBlockingTest {
            val movieListUiModel = MDBMovieListModel(
                results = listOf(MDBMovieModel())
            )
            val flow = flow { emit(movieListUiModel) }
            every { useCasePopularMovies.execute(any()) } returns flow
            viewModel.popularMoviesList.observeForever {}
            viewModel.getPopularMovies()

            val value = viewModel.popularMoviesList.getOrAwaitValue()
            assert(viewModel.popularMoviesList.hasObservers())
            assert(value.results.isNotEmpty())
            assert(value.error.not())
        }
    }

    @ExperimentalCoroutinesApi
    @Ignore("Temporary ignored to be able to generate Gitlab Page")
    @Test
    fun getUpcomingMoviesWithError() {
        runBlockingTest {
            val flow = flow {
                emit(MDBMovieListModel().apply {
                    setError(BaseModel.COMMON_ERROR)
                })
            }
            every { useCaseUpcomingMovies.execute(any()) } returns flow
            viewModel.upcomingMovies.observeForever {}

            viewModel.getUpcomingMovies()
            assert(viewModel.upcomingMovies.hasObservers())
            viewModel.upcomingMovies.value?.error?.let { assert(it) }
            viewModel.upcomingMovies.value?.errorCode?.let { assert(BaseModel.COMMON_ERROR == it) }
        }
    }
}
