package com.jlrf.mymovieapp.movie_detail

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.jlrf.base_provider.BaseModel
import com.jlrf.mymovieapp.extensions.getOrAwaitValue
import com.jlrf.mymovieapp.provider.moviesdb.movies.domain.interactor.UseCaseGetMovie
import com.jlrf.mymovieapp.provider.moviesdb.movies.domain.model.MDBMovieModel
import com.jlrf.mymovieapp.utils.mapper.UIDataMapper
import io.mockk.every
import io.mockk.mockk
import io.mockk.spyk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.newSingleThreadContext
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(
    JUnit4::class
)
internal class MovieDetailViewModelTest {

    private val useCase: UseCaseGetMovie = mockk()
    private val mapper: UIDataMapper = UIDataMapper()
    private val observer: Observer<MovieDetailDataUIModel> = mockk()
    private val mainThreadSurrogate = newSingleThreadContext("UI thread")

    private lateinit var viewModel: MovieDetailViewModel

    @Rule
    @JvmField
    var instantExecutorRule: InstantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setUp() {
        viewModel = spyk(MovieDetailViewModel(useCase, mapper), recordPrivateCalls = true)
        Dispatchers.setMain(mainThreadSurrogate)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain() // reset main dispatcher to the original Main dispatcher
        mainThreadSurrogate.close()
    }

    @Test
    fun getMovieDetailWithError() {
        runBlockingTest {
            val flow = flow {
                emit(MDBMovieModel().apply {
                    setError(BaseModel.COMMON_ERROR)
                })
            }
            every { useCase.execute(any()) } returns flow
            viewModel.movieDetail().observeForever(observer)

            viewModel.getMovieDetail(0)
            assert(viewModel.movieDetail().hasObservers())
            viewModel.movieDetail().value?.error?.let { assert(it) }
            viewModel.movieDetail().value?.errorCode?.let { assert(BaseModel.COMMON_ERROR == it) }
        }
    }

    @ExperimentalCoroutinesApi
    @Test
    fun getMovieDetail() {
        runBlockingTest {
            val flow = flow {
                emit(MDBMovieModel(1, TEST_NAME, TEST_NAME).apply {
                })
            }
            every { useCase.execute(any()) } returns flow
            viewModel.movieDetail().observeForever {
            }

            viewModel.getMovieDetail(1)
            assert(viewModel.movieDetail().hasObservers())
            val value = viewModel.movieDetail().getOrAwaitValue()
            assert(value.name == TEST_NAME)
            assert(value.error.not())
        }
    }

    companion object {
        const val TEST_NAME = "sharknado"
    }
}
