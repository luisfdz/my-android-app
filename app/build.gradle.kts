plugins {
    id(Plugins.jacocoMerged)
    id("com.android.application")
    id("androidx.navigation.safeargs")
    kotlin("android")
    kotlin("android.extensions")
    kotlin("kapt")
}

android {
    compileSdkVersion(Versions.compileSdk)
    buildToolsVersion(Versions.buildTools)
    dataBinding.isEnabled = true
    androidExtensions.isExperimental = true
    defaultConfig {
        applicationId = "com.jlrf.mymovieapp"
        minSdkVersion(Android.minSdkVersion)
        targetSdkVersion(Android.targetSdkVersion)
        versionCode = Android.versionCode
        versionName = Android.versionName
        vectorDrawables {
            useSupportLibrary = true
        }
    }

    configurations.all {
        resolutionStrategy.force("org.jetbrains:annotations:16.0.1")
    }

    buildTypes {
        getByName("debug") {
            isDebuggable = true
            isTestCoverageEnabled = true
        }
    }

    repositories {
        flatDir {
            dirs("libs")
        }
    }

    testOptions {
        animationsDisabled = true
        unitTests.apply {
            isReturnDefaultValues = true
            isIncludeAndroidResources = true
            all(KotlinClosure1<Any, Test>({
                (this as Test).also {
                    reports.junitXml.destination =
                        file("$buildDir/reports/${project.name}/test/")
                    reports.html.destination =
                        file("$buildDir/reports/${project.name}/test/")
                }
            }, this))
        }
        execution = "ANDROIDX_TEST_ORCHESTRATOR"
    }

    lintOptions {
        htmlReport = true
        xmlReport = true
        isAbortOnError = true
        setHtmlOutput(file("${parent?.buildDir}/reports/${project.name}/linter/lint-results.html"))
        setXmlOutput(file("${parent?.buildDir}/reports/${project.name}/linter/lint-results.xml"))
    }

    packagingOptions {
        exclude("META-INF/DEPENDENCIES")
        exclude("META-INF/LGPL2.1")
        exclude("META-INF/ASL2.0")
        exclude("META-INF/LICENSE")
        exclude("META-INF/NOTICE")
        exclude("META-INF/NOTICE.txt")
        exclude("META-INF/LICENSE.txt")
        exclude("META-INF/maven/com.squareup.picasso/picasso/pom.xml")
        exclude("META-INF/maven/com.squareup.picasso/picasso/pom.properties")
        exclude(".readme")
        exclude("asm-license.txt")
        exclude("cglib-license.txt")
        exclude("NOTICE")
        exclude("LICENSE")
        exclude("META-INF/services/javax.annotation.processing.Processor")
        exclude("META-INF/notice.txt")
        exclude("META-INF/ASL2.0")
        exclude("META-INF/atomicfu.kotlin_module")
    }

    compileOptions {
        setTargetCompatibility(1.8)
        setSourceCompatibility(1.8)
    }

    kotlinOptions {
        jvmTarget = JavaVersion.VERSION_1_8.toString()
    }
}

dependencies {
    implementation(fileTree("dir" to "libs", "include" to listOf("*.jar")))
    implementation(Libs.appCompat)
    implementation(project(Project.ui))
    implementation(Libs.multiDex)
    implementation(project(Project.movies_provider))
    kapt(Libs.roomCompiler)
    kapt(Libs.lifeCycleCompiler)
    kapt(Libs.daggerCompiler)
    kapt(Libs.daggerAndroidProcessor)

    testImplementation(TestLibs.jUnit)
    testImplementation(project(":base_tests"))
    testImplementation(TestLibs.mockk)
    testImplementation(TestLibs.coroutines)
    testImplementation(TestLibs.lifeCycleTest)

    androidTestImplementation(TestLibs.mockkAndroid)
    androidTestImplementation(TestLibs.testRunner)
    androidTestImplementation(TestLibs.testExt)
    androidTestImplementation(TestLibs.lifeCycleTest)
    androidTestImplementation(TestLibs.espressoCore)
    androidTestUtil(TestLibs.testOrchestrator)
}
