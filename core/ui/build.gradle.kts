plugins {
    id(Plugins.jacocoMerged)
    id("com.android.library")
    kotlin("android")
    kotlin("kapt")
    kotlin("android.extensions")
}
android {
    compileSdkVersion(Versions.compileSdk)
    buildToolsVersion(Versions.buildTools)
    defaultConfig {
        minSdkVersion(Android.minSdkVersion)
        targetSdkVersion(Android.targetSdkVersion)
        versionCode = Android.versionCode
        versionName = Android.versionName
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android.txt"), "proguard-rules.pro")
        }
        getByName("debug") {
            isMinifyEnabled = false
            isTestCoverageEnabled = true
        }
    }

    testOptions {
        animationsDisabled = true
        unitTests.apply {
            isReturnDefaultValues = true
            isIncludeAndroidResources = true
            all(KotlinClosure1<Any, Test>({
                (this as Test).also {
                    reports.junitXml.destination =
                        file("${parent?.buildDir}/reports/${project.name}/test/")
                    reports.html.destination =
                        file("${parent?.buildDir}/reports/${project.name}/test/")
                }
            }, this))
        }
        execution = "ANDROIDX_TEST_ORCHESTRATOR"
    }

    lintOptions {
        htmlReport = true
        xmlReport = true
        isAbortOnError = true
        setHtmlOutput(file("${parent?.buildDir}/reports/${project.name}/linter/lint-results.html"))
        setXmlOutput(file("${parent?.buildDir}/reports/${project.name}/linter/lint-results.xml"))
    }
}

dependencies {
    implementation(fileTree("dir" to "libs", "include" to listOf("*.jar")))
    implementation(Libs.kotlin)
    implementation(Libs.appCompat)
    implementation(Libs.core)
    api(Libs.constraintLayout)
    api(Libs.constraintLayoutSolver)
    api(Libs.navigationFragment)
    api(Libs.picasso)
    api(Libs.navigationUi)

    testImplementation(TestLibs.jUnit)
    testImplementation(TestLibs.coroutines)
    testImplementation(TestLibs.mockk)

    androidTestImplementation(TestLibs.mockkAndroid)
    androidTestImplementation(TestLibs.testRunner)
    androidTestImplementation(TestLibs.testExt)
    androidTestImplementation(TestLibs.espressoCore)

    androidTestUtil(TestLibs.testOrchestrator)
}
