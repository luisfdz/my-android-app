package com.jlrf.base_provider.utils

import java.util.Date
import org.joda.time.DateTime

interface TimeHelper {

    fun setup()

    fun convertISO8601ToUTC(inputDate: String): DateTime

    fun convertDateToString(date: Date?): String?

    fun convertDateTimeStringToDate(inputDate: String): DateTime

    fun convertDateTimeToString(date: DateTime): String

    val currentUTCDate: DateTime
}
