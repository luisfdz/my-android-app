package com.jlrf.base_provider.utils

object DomainErrorConstants {
    const val UNSYNCHRONIZED_DATA_ERROR = 1001
    const val NON_EXISTING_ERROR = 1002
    const val USER_NOT_AUTHENTICATED = 1008
    const val PARSING_ERROR = 1003
    const val SERVER_ERROR = 1007
    const val NETWORK_IO_ERROR = -1
    const val VALIDATION_ERROR = 1016
    const val UNKNOWN_ERROR = "An unknown error occurred!"
}
