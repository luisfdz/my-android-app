package com.jlrf.base_provider

import com.jlrf.base_provider.BaseModel.Companion.NO_VALUE
import com.jlrf.network.factory.Status
import javax.inject.Inject

open class BaseDataMapper @Inject constructor() {

    /**
     * Creates an error Model from a model type
     *
     * @param errorCode A custom error code associated
     * @param clazz The model type
     * @return [T] An instance of T type
     */
    fun <T : BaseModel> createDomainModel(
        errorCode: Int = NO_VALUE,
        clazz: Class<T>,
        status: Status = Status.LOADING
    ): T {
        val model = clazz.newInstance()
        if (errorCode != NO_VALUE || status == Status.ERROR) {
            model.setError(if (errorCode == NO_VALUE) BaseModel.COMMON_ERROR else errorCode)
        } else {
            model.status = convert(status)
        }
        return model
    }

    fun convert(status: Status): String = when (status) {
        Status.LOADING -> BaseModel.LOADING
        Status.SUCCESS -> BaseModel.SUCCESS
        Status.ERROR -> BaseModel.ERROR
    }
}
