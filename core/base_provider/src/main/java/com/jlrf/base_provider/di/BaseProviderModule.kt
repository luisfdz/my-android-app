package com.jlrf.base_provider.di

import com.jlrf.network.di.NetworkModule
import com.jlrf.storage.di.StorageModule
import dagger.Module

@Module(includes = [NetworkModule::class, StorageModule::class])
class BaseProviderModule
