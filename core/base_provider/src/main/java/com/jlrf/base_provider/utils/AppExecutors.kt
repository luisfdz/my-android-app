package com.jlrf.base_provider.utils

import android.os.Handler
import android.os.Looper
import java.util.concurrent.Executor
import java.util.concurrent.Executors
import javax.inject.Inject

const val THREAD_COUNT = 5

/**
 * Global executor pools for the whole application.
 *
 * Grouping tasks like this avoids the effects of task starvation (e.g. disk reads don't wait behind
 * webservice requests).
 */
open class AppExecutorsImpl @Inject constructor() : AppExecutors {

    private val diskIO: Executor
    private val networkIO: Executor
    private val mainThread: Executor

    init {
        diskIO = DiskIOThreadExecutor()
        networkIO = Executors.newFixedThreadPool(THREAD_COUNT)
        mainThread = MainThreadExecutor()
    }

    override fun getDiskThread(): Executor = diskIO

    override fun getNetworkThread(): Executor = networkIO

    override fun getMainThread(): Executor = mainThread
}

interface AppExecutors {

    fun getDiskThread(): Executor

    fun getNetworkThread(): Executor

    fun getMainThread(): Executor
}

class MainThreadExecutor @Inject constructor() : Executor {
    override fun execute(command: Runnable) {
        Handler(Looper.getMainLooper()).post(command)
    }
}

class DiskIOThreadExecutor @Inject constructor() : Executor {
    override fun execute(command: Runnable) {
        Executors.newSingleThreadExecutor().execute(command)
    }
}
