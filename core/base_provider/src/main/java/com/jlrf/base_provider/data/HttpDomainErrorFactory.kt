package com.jlrf.base_provider.data

interface HttpDomainErrorFactory {

    fun mapHttpError(httpErrorCode: Int): Int
}
