package com.jlrf.base_provider.utils

import android.content.Context
import java.text.ParseException
import java.util.Date
import javax.inject.Inject
import net.danlew.android.joda.JodaTimeAndroid
import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import org.joda.time.format.DateTimeFormat
import org.joda.time.format.ISODateTimeFormat

class TimeHelperImpl @Inject constructor(private val context: Context) : TimeHelper {

    override fun setup() {
        JodaTimeAndroid.init(context)
    }

    @Throws(ParseException::class)
    override fun convertISO8601ToUTC(inputDate: String): DateTime =
        ISODateTimeFormat.dateTime().withZone(DateTimeZone.UTC).parseDateTime(inputDate)

    override fun convertDateTimeStringToDate(inputDate: String): DateTime =
        DateTimeFormat.forPattern(ORDER_RESPONSE_DATE_FORMAT).parseDateTime(inputDate)

    override fun convertDateToString(date: Date?): String? =
        date?.let {
            ISODateTimeFormat.dateTime().withZone(DateTimeZone.UTC).print(it.time)
        }

    override fun convertDateTimeToString(date: DateTime): String = DateTimeFormat.forPattern(
        ORDER_DATE_FORMAT
    ).print(date)

    override val currentUTCDate: DateTime
        get() = DateTime.now(DateTimeZone.UTC)

    companion object {
        private const val ORDER_DATE_FORMAT = "MM/dd/yyyy"
        private const val ORDER_RESPONSE_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss"
    }
}
