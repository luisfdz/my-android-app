package com.jlrf.base_provider.data

import com.jlrf.base_provider.utils.DomainErrorConstants
import com.jlrf.network.factory.custom_callback.HttpErrorCodes
import javax.inject.Inject

class HttpDomainErrorFactoryImpl @Inject constructor() : HttpDomainErrorFactory {

    private val domainErrorsMap
        get() = hashMapOf(
            HttpErrorCodes.UNAUTHORIZED_ERROR to DomainErrorConstants.USER_NOT_AUTHENTICATED
        )

    override fun mapHttpError(httpErrorCode: Int): Int =
        domainErrorsMap[httpErrorCode] ?: DomainErrorConstants.SERVER_ERROR
}
