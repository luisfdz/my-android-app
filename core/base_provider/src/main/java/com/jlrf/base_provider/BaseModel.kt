package com.jlrf.base_provider

open class BaseModel constructor(
    var errorCode: Int = NO_VALUE,
    var error: Boolean = false,
    var status: String = LOADING,
    var baseErrorMessage: String = EMPTY_STRING
) {

    fun setError(errorCode: Int, errorMessage: String = EMPTY_STRING) {
        this.errorCode = errorCode
        this.error = true
        this.status = ERROR
        this.baseErrorMessage = errorMessage
    }

    fun setSuccess() {
        this.status = SUCCESS
        this.errorCode = NO_VALUE
        this.error = false
        this.baseErrorMessage = EMPTY_STRING
    }

    companion object {
        const val LOADING = "LOADING"
        const val SUCCESS = "SUCCESS"
        const val ERROR = "ERROR"
        const val EMPTY_STRING = ""
        const val EMPTY_INT = 0
        const val EMPTY_LONG = 0L
        const val EMPTY_BOOLEAN = false
        const val EMPTY_FLOAT = 0f
        const val EMPTY_DOUBLE = 0.0
        const val NO_VALUE = 0
        const val COMMON_ERROR = -1

        /**
         * Shallow copy of base class [BaseModel]
         *
         * @param base [BaseModel] base class
         * @return [T] sub class
         */
        fun <T : BaseModel> T.baseCopy(base: BaseModel): T {
            status = base.status
            error = base.error
            errorCode = base.errorCode
            return this
        }
    }
}
