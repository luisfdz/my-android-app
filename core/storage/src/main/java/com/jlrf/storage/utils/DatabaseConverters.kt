package com.jlrf.storage.utils

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.util.Date
import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import org.joda.time.format.ISODateTimeFormat

object DatabaseConverters {

    @TypeConverter
    @JvmStatic
    fun convertToDate(dateToConvert: String?): Date? =
        dateToConvert?.let {
            ISODateTimeFormat.dateTime().withZone(DateTimeZone.UTC).parseDateTime(it).toDate()
        }

    @TypeConverter
    @JvmStatic
    fun convertToString(date: Date?): String? =
        if (date == null) null else DateTime(date).toString()

    @TypeConverter
    @JvmStatic
    fun stringListToJson(value: List<String>?): String = Gson().toJson(value)

    @TypeConverter
    @JvmStatic
    fun jsonToListString(value: String): List<String>? {
        val listType = object : TypeToken<List<String>>() {}.type
        return Gson().fromJson(value, listType)
    }

    @TypeConverter
    @JvmStatic
    fun intListToJson(value: List<Int>?): String = Gson().toJson(value)

    @TypeConverter
    @JvmStatic
    fun jsonToIntList(value: String): List<Int>? {
        val listType = object : TypeToken<List<Int>>() {}.type
        return Gson().fromJson(value, listType)
    }
}
