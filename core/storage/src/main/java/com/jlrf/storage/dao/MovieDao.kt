package com.jlrf.storage.dao

import androidx.room.Dao
import androidx.room.Query
import com.jlrf.storage.entities.MovieEntity
import com.jlrf.storage.utils.DatabaseConstants

@Dao
interface MovieDao : BaseDao<MovieEntity> {

    @Query(value = "select * from ${DatabaseConstants.TABLE_MOVIE} where ${DatabaseConstants.COLUMN_ID} = :movieId")
    suspend fun loadMovieById(movieId: Int): MovieEntity?

    @Query(value = "select * from ${DatabaseConstants.TABLE_MOVIE}")
    suspend fun loadAllMovies(): List<MovieEntity>?
}
