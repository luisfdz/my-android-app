package com.jlrf.storage

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.jlrf.storage.dao.MovieCategoryDao
import com.jlrf.storage.dao.MovieDao
import com.jlrf.storage.entities.MovieCategoryEntity
import com.jlrf.storage.entities.MovieEntity
import com.jlrf.storage.utils.DatabaseConstants.DATABASE_VERSION
import com.jlrf.storage.utils.DatabaseConverters

@Database(
    entities = [MovieEntity::class, MovieCategoryEntity::class],
    version = DATABASE_VERSION
)
@TypeConverters(DatabaseConverters::class)
abstract class MDBDatabase : RoomDatabase() {

    abstract fun movieDao(): MovieDao

    abstract fun movieCategoryDao(): MovieCategoryDao
}
