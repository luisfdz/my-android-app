package com.jlrf.storage.entities

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.jlrf.storage.utils.DatabaseConstants

@Entity(tableName = DatabaseConstants.TABLE_MOVIE_CATEGORY)
data class MovieCategoryEntity(
    @NonNull
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    val id: Int = 0,
    @ColumnInfo(name = "movie_id")
    val movie_id: Int = 0,
    @ColumnInfo(name = "category_id")
    val category_id: Int = 0
)
