package com.jlrf.storage.entities

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.jlrf.storage.utils.DatabaseConstants
import java.util.Date

@Entity(tableName = DatabaseConstants.TABLE_MOVIE)
data class MovieEntity(
    @NonNull
    @PrimaryKey
    @ColumnInfo(name = "id")
    val movie_id: Int = 0,
    @ColumnInfo(name = "title")
    val title: String = "",
    @ColumnInfo(name = "movie_status")
    val status: String = "",
    @ColumnInfo(name = "poster_path")
    val posterPath: String = "",
    @ColumnInfo(name = "backdrop_path")
    val backdropPath: String = "",
    @ColumnInfo(name = "video")
    val video: Boolean = false,
    @ColumnInfo(name = "imdb_id")
    val imdbId: String = "",
    @ColumnInfo(name = "home_page")
    val homePage: String = "",
    @ColumnInfo(name = "adult")
    val adult: Boolean = false,
    @ColumnInfo(name = "tag_line")
    val tagline: String = "",
    @ColumnInfo(name = "vote_average")
    val voteAverage: Double = 0.0,
    @ColumnInfo(name = "release_date")
    val releaseDate: Date? = null,
    @ColumnInfo(name = "runTime")
    val runTime: Int = 0,
    @ColumnInfo(name = "original_title")
    val originalTitle: String = "",
    @ColumnInfo(name = "overview")
    val overview: String = "",
    @ColumnInfo(name = "budget")
    val budget: Int = 0,
    @ColumnInfo(name = "vote_count")
    val voteCount: Int = 0,
    @ColumnInfo(name = "popularity")
    val popularity: Double = 0.0,
    @ColumnInfo(name = "genre_ids")
    val genreIds: List<Int> = listOf(),
    @ColumnInfo(name = "original_language")
    val originalLanguage: String = ""
)
