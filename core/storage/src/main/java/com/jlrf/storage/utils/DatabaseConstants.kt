package com.jlrf.storage.utils

object DatabaseConstants {
    const val DATABASE_NAME_PROPERTY = "mdb_database_name"
    const val DATABASE_NAME = "mymovieapp_app.db"
    const val DATABASE_VERSION = 2
    const val TABLE_MOVIE = "movies"
    const val TABLE_MOVIE_CATEGORY = "movie_category"
    const val COLUMN_ID = "id"
    const val COLUMN_CATEGORY_ID = "category_id"
}
