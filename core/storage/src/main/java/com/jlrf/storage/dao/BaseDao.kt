package com.jlrf.storage.dao

import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Update

interface BaseDao<T> {

    @Insert(onConflict = REPLACE)
    suspend fun insert(entity: T): Long

    @Insert(onConflict = REPLACE)
    suspend fun insertAll(entities: List<T>): List<Long>

    @Insert(onConflict = REPLACE)
    suspend fun upsert(entity: T): Long

    @Update(onConflict = REPLACE)
    suspend fun update(entity: T): Int

    @Delete
    suspend fun delete(entity: T)
}
