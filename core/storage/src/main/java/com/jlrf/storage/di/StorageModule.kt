package com.jlrf.storage.di

import android.content.Context
import androidx.room.Room
import com.jlrf.storage.MDBDatabase
import com.jlrf.storage.utils.DatabaseConstants.DATABASE_NAME
import com.jlrf.storage.utils.DatabaseConstants.DATABASE_NAME_PROPERTY
import dagger.Module
import dagger.Provides
import javax.inject.Named

@Module
class StorageModule {
    @Provides
    fun providesDataBase(
        context: Context,
        @Named(DATABASE_NAME_PROPERTY) databaseName: String = DATABASE_NAME
    ): MDBDatabase =
        Room.databaseBuilder(context, MDBDatabase::class.java, databaseName).build()
}
