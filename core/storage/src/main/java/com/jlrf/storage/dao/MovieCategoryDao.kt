package com.jlrf.storage.dao

import androidx.room.Dao
import androidx.room.Query
import com.jlrf.storage.entities.MovieCategoryEntity
import com.jlrf.storage.utils.DatabaseConstants

@Dao
interface MovieCategoryDao : BaseDao<MovieCategoryEntity> {

    @Query(value = "select * from ${DatabaseConstants.TABLE_MOVIE_CATEGORY} where ${DatabaseConstants.COLUMN_CATEGORY_ID} = :categoryId")
    suspend fun loadCategoryById(categoryId: Int): List<MovieCategoryEntity>?
}
