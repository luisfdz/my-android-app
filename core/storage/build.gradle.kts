plugins {
    id(Plugins.jacocoMerged)
    id("com.android.library")
    kotlin("android")
    kotlin("kapt")
    kotlin("android.extensions")
}
android {
    compileSdkVersion(Versions.compileSdk)
    buildToolsVersion(Versions.buildTools)
    defaultConfig {
        minSdkVersion(Android.minSdkVersion)
        targetSdkVersion(Android.targetSdkVersion)
        versionCode = Android.versionCode
        versionName = Android.versionName
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"

        kapt {
            arguments {
                arg("room.schemaLocation", "$projectDir/schemas")
                arg("room.incremental", "true")
                arg("room.expandProjection", "true")
            }
        }
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android.txt"), "proguard-rules.pro")
        }
        getByName("debug") {
            isMinifyEnabled = false
            isTestCoverageEnabled = true
        }
    }

    testOptions {
        animationsDisabled = true
        unitTests.apply {
            isReturnDefaultValues = true
            isIncludeAndroidResources = true
            all(KotlinClosure1<Any, Test>({
                (this as Test).also {
                    reports.junitXml.destination =
                        file("${parent?.buildDir}/reports/${project.name}/test/")
                    reports.html.destination =
                        file("${parent?.buildDir}/reports/${project.name}/test/")
                }
            }, this))
        }
        execution = "ANDROIDX_TEST_ORCHESTRATOR"
    }

    lintOptions {
        htmlReport = true
        xmlReport = true
        isAbortOnError = true
        setHtmlOutput(file("${parent?.buildDir}/reports/${project.name}/linter/lint-results.html"))
        setXmlOutput(file("${parent?.buildDir}/reports/${project.name}/linter/lint-results.xml"))
    }
}

dependencies {
    implementation(fileTree("dir" to "libs", "include" to listOf("*.jar")))
    implementation(Libs.kotlin)
    implementation(Libs.kotlinReflect)
    implementation(Libs.dagger)
    implementation(Libs.daggerAndroidSupport)
    api(Libs.lifeCycleExtensions)
    api(Libs.lifeCycleCommon)
    api(Libs.lifeCycleViewModel)
    api(Libs.lifeCycleLiveData)
    implementation(Libs.appCompat)
    implementation(Libs.core)
    api(Libs.coroutines)
    api(Libs.coroutinesAndroid)
    api(Libs.coroutinesPlay)
    implementation(Libs.gson)
    api(Libs.archRoomTime)
    api(Libs.roomCoroutines)
    implementation(Libs.jodaTimes)

    kapt(Libs.roomCompiler)
    kapt(Libs.lifeCycleCompiler)
    kapt(Libs.daggerCompiler)
    kapt(Libs.daggerAndroidProcessor)

    testImplementation(TestLibs.jUnit)
    testImplementation(TestLibs.roomTesting)
    testImplementation(TestLibs.mockk)
    testImplementation(TestLibs.coroutines)

    androidTestImplementation(TestLibs.testRunner)
    androidTestImplementation(TestLibs.testExt)
    androidTestImplementation(TestLibs.espressoCore)
    androidTestUtil(TestLibs.testOrchestrator)
}
