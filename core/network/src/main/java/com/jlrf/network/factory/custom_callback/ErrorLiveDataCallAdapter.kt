package com.jlrf.network.factory.custom_callback

import java.lang.reflect.Type
import retrofit2.Call
import retrofit2.CallAdapter
import retrofit2.Retrofit

class ErrorLiveDataCallAdapter<R, E>(
    private val responseType: Type,
    private val errorType: Type,
    private val annotations: Array<Annotation>,
    private val retrofit: Retrofit
) : CallAdapter<R, LiveDataResponseCall<R, E>> {

    override fun adapt(call: Call<R>): LiveDataResponseCall<R, E>? =
        LiveDataResponseCall<R, E>(
            call,
            retrofit,
            errorType,
            annotations
        )

    override fun responseType(): Type = responseType
}
