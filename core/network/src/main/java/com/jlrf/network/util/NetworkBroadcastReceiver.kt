package com.jlrf.network.util

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import androidx.annotation.NonNull

/**
 * A BroadcastReceiver which monitors the device's connectivity state by listening for
 * {@link ConnectivityManager#CONNECTIVITY_ACTION} broadcasts, and notifies its listener.
 * <p>
 * Note that in API level 26 the CONNECTIVITY_ACTION broadcast will not be delivered implicitly (i.e. to
 * receivers that are only declared in the manifest). You must explicitly register and unregister this class
 * using the provided methods to receive the broadcast.
 */
class NetworkBroadcastReceiver(@NonNull val listener: NetworkListener) : BroadcastReceiver() {

    interface NetworkListener {
        fun onNetworkLost()
        fun onNetworkAvailable()
    }

    override fun onReceive(context: Context, intent: Intent) {
        if (ConnectivityManager.CONNECTIVITY_ACTION == intent.action) {
            if (intent.getBooleanExtra(ConnectivityManager.EXTRA_NO_CONNECTIVITY, false)) {
                listener.onNetworkLost()
            } else {
                val network =
                    (context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager).activeNetworkInfo
                if (network?.isConnected == true) listener.onNetworkAvailable() else listener.onNetworkLost()
            }
        } else {
            throw IllegalStateException(
                "${NetworkBroadcastReceiver::class.simpleName} should not be receiving broadcasts with an Action other than ${ConnectivityManager.CONNECTIVITY_ACTION}"
            )
        }
    }

    fun register(context: Context) {
        context.registerReceiver(this, this.intentFilter())
    }

    fun unregister(context: Context) {
        context.unregisterReceiver(this)
    }

    private fun intentFilter(): IntentFilter = IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION)
}
