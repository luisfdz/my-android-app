package com.jlrf.network.factory.custom_callback

import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type
import javax.inject.Inject
import retrofit2.CallAdapter
import retrofit2.Retrofit

class ErrorLiveDataCallAdapterFactory @Inject constructor() : CallAdapter.Factory() {

    override fun get(
        returnType: Type,
        annotations: Array<Annotation>,
        retrofit: Retrofit
    ): CallAdapter<*, *>? {
        if (getRawType(returnType) != LiveDataResponseCall::class.java) {
            return null
        }

        val observableType = getParameterUpperBound(
            FIRST_GENERIC_ARGUMENT,
            returnType as ParameterizedType
        )

        val rawObservableType = getRawType(observableType)

        require(rawObservableType == LiveDataResponseCall::class.java) { "Type must be an ApiResponse class" }

        require(observableType is ParameterizedType) { "ApiResponse must be parametrized" }

        val responseType = getParameterUpperBound(
            FIRST_GENERIC_ARGUMENT,
            observableType
        )

        val errorType = getParameterUpperBound(
            SECOND_GENERIC_ARGUMENT,
            observableType
        )

        return ErrorLiveDataCallAdapter<Any, Any>(
            responseType,
            errorType,
            annotations,
            retrofit
        )
    }

    companion object {

        fun create(): ErrorLiveDataCallAdapterFactory =
            ErrorLiveDataCallAdapterFactory()

        private const val FIRST_GENERIC_ARGUMENT = 0
        private const val SECOND_GENERIC_ARGUMENT = 1
    }
}
