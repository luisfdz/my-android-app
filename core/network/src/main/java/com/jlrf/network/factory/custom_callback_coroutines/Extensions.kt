package com.jlrf.network.factory.custom_callback_coroutines

import java.io.IOException
import kotlinx.coroutines.delay
import okhttp3.ResponseBody
import retrofit2.Converter
import retrofit2.HttpException

/**
 * Retries the given [block] for the specified number of times in the case of [DeferredResponse.NetworkError]
 *
 * @param S The success body type of [DeferredResponse]
 * @param E The error body type of [DeferredResponse]
 * @param times The number of times this request should be retried
 * @param initialDelay The initial amount of time to wait before retrying
 * @param maxDelay The max amount of time to wait before retrying
 * @param factor Multiply current delay time with this on each retry
 * @param block The suspending function to be retried
 * @return The Response value whether it be successful or failed after retrying
 */
suspend inline fun <S, E> executeWithRetry(
    times: Int = 10,
    initialDelay: Long = 100, // 0.1 second
    maxDelay: Long = 1000, // 1 second
    factor: Double = 2.0,
    block: () -> DeferredResponse<S, E>
): DeferredResponse<S, E> {
    var currentDelay = initialDelay
    repeat(times - 1) {
        when (val response = block()) {
            is DeferredResponse.NetworkError -> {
                delay(currentDelay)
                currentDelay = (currentDelay * factor).toLong().coerceAtMost(maxDelay)
            }
            else -> return response
        }
    }
    return block() // last attempt
}

/**
 * Overloaded invoke operator to get the successful body or null in Response class
 *
 * @param S the success body type of [DeferredResponse]
 * @param E the error body type of [DeferredResponse]
 *
 * Example:
 * val usersResponse = executeWithRetry { getUsers() }
 *
 * println(usersResponse() ?: "No users found")
 */
operator fun <S, E> DeferredResponse<S, E>.invoke(): S? {
    return if (this is DeferredResponse.Success) body else null
}

internal const val UNKNOWN_ERROR_RESPONSE_CODE = 520

internal fun <S, E> HttpException.extractFromHttpException(errorConverter: Converter<ResponseBody, E>): DeferredResponse<S, E> {
    val error = response()?.errorBody()
    val responseCode = response()?.code() ?: UNKNOWN_ERROR_RESPONSE_CODE
    val headers = response()?.headers()
    val errorBody = when {
        error == null -> null // No error content available
        error.contentLength() == 0L -> null // Error content is empty
        else -> try {
            // There is error content present, so we should try to extract it
            errorConverter.convert(error)
        } catch (e: Exception) {
            // If unable to extract content, return with a null body and don't parse further
            return DeferredResponse.ServerError(body = null, code = responseCode, headers = headers)
        }
    }
    return DeferredResponse.ServerError(
        errorBody = errorBody,
        code = responseCode,
        headers = headers
    )
}

internal fun <S, E> Throwable.extractNetworkResponse(errorConverter: Converter<ResponseBody, E>): DeferredResponse<S, E> {
    return when (this) {
        is IOException -> DeferredResponse.NetworkError(this)
        is HttpException -> extractFromHttpException<S, E>(errorConverter)
        else -> throw this
    }
}
