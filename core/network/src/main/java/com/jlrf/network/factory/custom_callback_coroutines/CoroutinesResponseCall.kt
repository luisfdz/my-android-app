package com.jlrf.network.factory.custom_callback_coroutines

import okhttp3.Request
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Converter
import retrofit2.Response

internal class CoroutinesResponseCall<S, E>(
    private val backingCall: Call<S>,
    private val errorConverter: Converter<ResponseBody, E>
) : Call<DeferredResponse<S, E>> {

    override fun enqueue(callback: Callback<DeferredResponse<S, E>>) = synchronized(this) {
        backingCall.enqueue(object : Callback<S> {
            override fun onResponse(call: Call<S>, response: Response<S>) {
                val body = response.body()
                val headers = response.headers()
                val code = response.code()
                val errorBody = response.errorBody()

                if (response.isSuccessful) {
                    if (body != null) {
                        callback.onResponse(
                            this@CoroutinesResponseCall,
                            Response.success(DeferredResponse.Success(body, headers))
                        )
                    } else {
                        // Response is successful but the body is null, so there's probably a server error here
                        callback.onResponse(
                            this@CoroutinesResponseCall,
                            Response.success(
                                DeferredResponse.ServerError(
                                    code = code,
                                    headers = headers
                                )
                            )
                        )
                    }
                } else {
                    val convertedErrorBody = try {
                        if (errorBody != null)
                            errorConverter.convert(errorBody)
                        else null
                    } catch (ex: Exception) {
                        null
                    }
                    callback.onResponse(
                        this@CoroutinesResponseCall,
                        Response.success(
                            DeferredResponse.ServerError(
                                errorBody = convertedErrorBody,
                                code = code,
                                headers = headers
                            )
                        )
                    )
                }
            }

            override fun onFailure(call: Call<S>, throwable: Throwable) {
                val response = throwable.extractNetworkResponse<S, E>(errorConverter)
                callback.onResponse(this@CoroutinesResponseCall, Response.success(response))
            }
        })
    }

    override fun isExecuted(): Boolean = synchronized(this) {
        backingCall.isExecuted
    }

    override fun clone(): Call<DeferredResponse<S, E>> =
        CoroutinesResponseCall(backingCall.clone(), errorConverter)

    override fun isCanceled(): Boolean = synchronized(this) {
        backingCall.isCanceled
    }

    override fun cancel() = synchronized(this) {
        backingCall.cancel()
    }

    override fun execute(): Response<DeferredResponse<S, E>> =
        throw UnsupportedOperationException("Network Response call does not support synchronous execution")

    override fun request(): Request = backingCall.request()
}
