package com.jlrf.network.factory

import com.jlrf.network.BuildConfig
import com.itkacher.okhttpprofiler.OkHttpProfilerInterceptor
import java.util.concurrent.TimeUnit
import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import retrofit2.CallAdapter
import retrofit2.Converter
import retrofit2.Retrofit

object ServiceFactory {

    fun buildRetrofit(
        url: HttpUrl,
        client: OkHttpClient,
        converter: Converter.Factory,
        adapter: CallAdapter.Factory
    ): Retrofit =
        Retrofit.Builder()
            .baseUrl(url)
            .addCallAdapterFactory(adapter)
            .addConverterFactory(converter)
            .client(client)
            .build()

    fun buildOkHttpClient(tokenInterceptor: AuthTokenInterceptor): OkHttpClient =
        OkHttpClient.Builder()
            .addInterceptor(tokenInterceptor)
            .readTimeout(MAX_CONNECTION_TIME_OUT_SECONDS, TimeUnit.SECONDS)
            .connectTimeout(MAX_READ_TIME_OUT_SECONDS, TimeUnit.SECONDS)
            .apply {
                if (BuildConfig.DEBUG) {
                    addInterceptor(OkHttpProfilerInterceptor())
                }
            }
            .build()

    fun providesHttpUrl(host: String): HttpUrl =
        HttpUrl.Builder()
            .scheme(SCHEME)
            .host(host)
            .build()

    private const val MAX_READ_TIME_OUT_SECONDS = 60L
    private const val MAX_CONNECTION_TIME_OUT_SECONDS = 60L
    private const val SCHEME = "https"
    const val HOST = "api.themoviedb.org"
    const val MDB_API_KEY_VALUE = "b2ba669b4bbc3fec8ccd24924c8dccc4"
}
