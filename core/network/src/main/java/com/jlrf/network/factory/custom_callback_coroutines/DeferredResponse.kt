package com.jlrf.network.factory.custom_callback_coroutines

import java.io.IOException
import okhttp3.Headers

sealed class DeferredResponse<S, E> {
    /**
     * A request that resulted in a response with a 2xx status code that has a body.
     */
    data class Success<S, E>(val body: S, val headers: Headers? = null) :
        DeferredResponse<S, E>()

    /**
     * A request that resulted in a response with a non-2xx status code.
     */
    data class ServerError<S, E>(
        val body: S? = null,
        val errorBody: E? = null,
        val code: Int,
        val headers: Headers? = null
    ) : DeferredResponse<S, E>()

    /**
     * A request that didn't result in a response.
     */
    data class NetworkError<S, E>(val error: IOException) : DeferredResponse<S, E>()
}
