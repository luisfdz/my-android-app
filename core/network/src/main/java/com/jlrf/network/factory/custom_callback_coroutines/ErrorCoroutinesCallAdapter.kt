package com.jlrf.network.factory.custom_callback_coroutines

import java.lang.reflect.Type
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.CallAdapter
import retrofit2.Converter

class ErrorCoroutinesCallAdapter<S, E>(
    private val successType: Type,
    private val errorBodyConverter: Converter<ResponseBody, E>
) : CallAdapter<S, Call<DeferredResponse<S, E>>> {

    override fun responseType(): Type = successType

    override fun adapt(call: Call<S>): Call<DeferredResponse<S, E>> =
        CoroutinesResponseCall(call, errorBodyConverter)
}
