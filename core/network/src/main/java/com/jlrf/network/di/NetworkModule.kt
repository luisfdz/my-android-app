package com.jlrf.network.di

import com.jlrf.network.factory.AuthTokenInterceptor
import com.jlrf.network.factory.ServiceFactory
import com.jlrf.network.factory.custom_callback_coroutines.ErrorCoroutinesCallAdapterFactory
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import javax.inject.Named
import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import retrofit2.CallAdapter
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@Module
class NetworkModule {

    companion object {
        const val SCHEME = "https"
        const val MOVIES_HOST_NAME = "movie_db_host"
        const val MDB_API = "mdb_api_key"
        const val GSON_CONVERTER = "retrofit_gson"
        private const val SSL_TYPE = "SSL"
    }

    @Provides
    @Named(MOVIES_HOST_NAME)
    fun provideHost(): String = ServiceFactory.HOST

    @Provides
    @Named(MDB_API)
    fun providesMDBApiKey(): String = ServiceFactory.MDB_API_KEY_VALUE

    @Provides
    fun provideHttpUrl(
        @Named(MOVIES_HOST_NAME) host: String
    ): HttpUrl = ServiceFactory.providesHttpUrl(host)

    @Provides
    fun provideMDBApiTokenInterceptor(@Named(MDB_API) apiKey: String): AuthTokenInterceptor =
        AuthTokenInterceptor(
            apiKey
        )

    @Provides
    fun provideOkHttpClient(
        tokenInterceptor: AuthTokenInterceptor
    ): OkHttpClient = ServiceFactory.buildOkHttpClient(tokenInterceptor)

    @Provides
    @Named(GSON_CONVERTER)
    fun provideGson(): Gson = GsonBuilder().create()

    @Provides
    fun provideJsonConverterFactory(@Named(GSON_CONVERTER) gson: Gson): Converter.Factory =
        GsonConverterFactory.create(gson)

    @Provides
    fun provideCallAdapterFactory(): CallAdapter.Factory =
        ErrorCoroutinesCallAdapterFactory.create()

    @Provides
    fun providesRetrofit(
        httpUrl: HttpUrl,
        httpClient: OkHttpClient,
        converter: Converter.Factory,
        adapter: CallAdapter.Factory
    ): Retrofit =
        ServiceFactory.buildRetrofit(httpUrl, httpClient, converter, adapter)
}
