package com.jlrf.network.factory

import com.jlrf.network.factory.custom_callback.HttpErrorCodes.EXCEPTION_RESPONSE_CODE
import com.jlrf.network.factory.custom_callback.HttpErrorCodes.NO_NETWORK_RESPONSE_CODE
import com.jlrf.network.factory.custom_callback_coroutines.DeferredResponse

abstract class BaseCloudDataSource {

    protected suspend fun <T, U> getResultCoroutines(call: suspend () -> DeferredResponse<T, U>): ResourceCoroutines<T, U> =
        try {
            when (val response = call()) {
                // Handle Success
                is DeferredResponse.Success ->
                    ResourceCoroutines.success(data = response.body)
                // Handle Server Error
                is DeferredResponse.ServerError ->
                    ResourceCoroutines.error(
                        errorCode = response.code,
                        errorBody = response.errorBody
                    )
                // Handle Network Error
                is DeferredResponse.NetworkError ->
                    ResourceCoroutines.error(NO_NETWORK_RESPONSE_CODE)
            }
        } catch (e: Exception) {
            ResourceCoroutines.error(EXCEPTION_RESPONSE_CODE)
        }
}
