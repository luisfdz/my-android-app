package com.jlrf.network.factory

/**
 * A generic class that holds a value with its loading status.
 * @param <S>
 * @param <E>
 */
class ResourceCoroutines<S, E>(
    val status: Status,
    val body: S? = null,
    val errorBody: E? = null,
    val errorCode: Int = 0
) {

    override fun equals(other: Any?): Boolean {
        if (this === other) {
            return true
        }
        if (other == null || javaClass != other.javaClass) {
            return false
        }

        val resource = other as ResourceCoroutines<*, *>?

        if (status !== resource!!.status) {
            return false
        }
        if (if (errorCode != 0) errorCode != resource?.errorCode else resource?.errorCode != null) {
            return false
        }
        return if (body != null) body == resource?.body else resource?.body == null
    }

    override fun hashCode(): Int {
        var result = status.hashCode()
        result = 31 * result + errorCode.hashCode()
        result = 31 * result + (body?.hashCode() ?: 0)
        return result
    }

    override fun toString(): String =
        "Resource {status= $status , errorCode= $errorCode, data= $body  }"

    companion object {

        fun <T, U> success(data: T): ResourceCoroutines<T, U> {
            return ResourceCoroutines(
                Status.SUCCESS,
                data
            )
        }

        fun <T, U> error(
            errorCode: Int,
            body: T? = null,
            errorBody: U? = null
        ): ResourceCoroutines<T, U> {
            return ResourceCoroutines(
                status = Status.ERROR,
                body = body,
                errorBody = errorBody,
                errorCode = errorCode
            )
        }

        fun <T, U> loading(data: T): ResourceCoroutines<T, U> {
            return ResourceCoroutines(
                Status.LOADING,
                data
            )
        }
    }
}

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}
