package com.jlrf.network.factory.custom_callback_coroutines

import java.lang.reflect.Type
import kotlinx.coroutines.CompletableDeferred
import kotlinx.coroutines.Deferred
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.CallAdapter
import retrofit2.Callback
import retrofit2.Converter
import retrofit2.Response

/**
 * A Retrofit converter to return objects wrapped in [DeferredResponse] class
 *
 * @param S The type of the successful response model
 * @param E The type of the error response model
 * @param successBodyType The type of the successful response model in [DeferredResponse]
 * @param errorConverter The converter to extract error information from [ResponseBody]
 * @constructor Creates a DeferredNetworkResponseAdapter
 */

internal class DeferredResponseAdapter<S, E>(
    private val successBodyType: Type,
    private val errorConverter: Converter<ResponseBody, E>
) : CallAdapter<S, Deferred<DeferredResponse<S, E>>> {

    /**
     * This is used to determine the parameterize type of the object
     * being handled by this adapter. For example, the response type
     * in Call<Repo> is Repo.
     */
    override fun responseType(): Type = successBodyType

    /**
     * Returns an instance of [T] by modifying a [Call] object
     *
     * @param call The call object to be converted
     * @return The T instance wrapped in a [DeferredResponse] class wrapped in [Deferred]
     */
    override fun adapt(call: Call<S>): Deferred<DeferredResponse<S, E>> {
        val deferred = CompletableDeferred<DeferredResponse<S, E>>()

        deferred.invokeOnCompletion {
            if (deferred.isCancelled) {
                call.cancel()
            }
        }

        call.enqueue(object : Callback<S> {
            override fun onFailure(call: Call<S>, throwable: Throwable) {
                try {
                    val networkResponse = throwable.extractNetworkResponse<S, E>(errorConverter)
                    deferred.complete(networkResponse)
                } catch (t: Throwable) {
                    deferred.completeExceptionally(t)
                }
            }

            override fun onResponse(call: Call<S>, response: Response<S>) {
                val headers = response.headers()
                val responseCode = response.code()
                val body = response.body()
                body?.let {
                    deferred.complete(DeferredResponse.Success(it, headers))
                } ?: deferred.complete(
                    DeferredResponse.ServerError(
                        code = responseCode,
                        headers = headers
                    )
                )
            }
        })

        return deferred
    }
}
