plugins {
    id("java-library")
    id("kotlin")
}



dependencies {
    implementation(fileTree("dir" to "libs", "include" to listOf("*.jar")))
    implementation(Libs.kotlin)
    implementation(Libs.kotlinReflect)
    implementation(TestLibs.jUnit)
    implementation(TestLibs.mockk)
    implementation(TestLibs.coroutines)
    implementation(Libs.gson)
}