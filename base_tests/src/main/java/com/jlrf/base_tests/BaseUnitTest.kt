package com.jlrf.base_tests

import com.google.gson.Gson
import java.lang.reflect.Type

@Suppress("UNCHECKED_CAST")
abstract class BaseUnitTest {

    inline fun <reified T : Any> readResourceFile(fileName: String, directory: String = "api-response"): T {
        val jsonResponse = javaClass.classLoader?.getResourceAsStream("$directory/$fileName")
            ?.bufferedReader().use { it?.readText() }
        return Gson().fromJson(jsonResponse, T::class.java)
    }

    fun <T> readResourceFile(fileName: String, type: Type, directory: String = "api-response"): T {
        val jsonResponse = javaClass.classLoader?.getResourceAsStream("$directory/$fileName")
            ?.bufferedReader().use { it?.readText() }
        return Gson().fromJson(jsonResponse, type)
    }

    fun readResourceFile(fileName: String, directory: String = "api-response"): String =
        javaClass.classLoader?.getResourceAsStream("$directory/$fileName")
            ?.bufferedReader().use { it?.readText() } ?: ""

    fun <T> List<T>.createList() = listOf<T>()

    companion object {
        const val TEST_STRING = "string"
        const val TEST_INT = 0
        const val TEST_BOOLEAN = true
        const val TEST_LONG = 0L
        const val TEST_DOUBLE = 0.0
        const val TEST_SIZE = 1
    }
}
