rootProject.buildFileName = "build.gradle.kts"
include("app")
include("core:analytics")
include("core:network")
include("core:ui")
include("core:storage")
include("core:base")
include("core:base_provider")
include(":base_tests")
include("feature_provider:moviesdb_movies_provider")